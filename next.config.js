/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['indihome.co.id', 'subsystem.indihome.co.id'],
  },
};

module.exports = nextConfig;
