/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        banner: 'url(/assets/img/banner-desktop1.jpg)',
        'footer-texture': "url('/img/footer-texture.png')",
        mobilebanner: 'url(/assets/img/banner-model1.jpg)',
        mobilebanner2: 'url(/assets/img/banner-model2.png)',
        waveredmobile: 'url(/assets/img/wave-red-mobile.png)',
        bannerinternet: 'url(/assets/img/bg_product_internet.jpg)',
        banner2: 'url(/assets/img/banner-desktop2.png)',
        homebanner: 'url(/assets/img/banner-home.png)',
      },
      maxWidth: {
        40: '40%',
      },
      backgroundSize: {
        '100%': '100%',
      },
    },
  },
  plugins: [],
};
