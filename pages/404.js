import Layout from '../components/Layout';
import Image from 'next/image';

const NotFoundPage = () => (
  <Layout title="404">
    <main className="h-[35.1rem] w-full flex flex-col justify-center items-center">
      <Image
        src="/assets/img/blank_default.svg"
        alt="blank"
        width={250}
        height={250}
        className=""
      />
    </main>
  </Layout>
);
export default NotFoundPage;
