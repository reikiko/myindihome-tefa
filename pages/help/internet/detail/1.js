import Layout from '../../../../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
export default function PageServices() {
  return (
    <Layout>
      <div className="bg-[url('/assets/img/bg_header.png')] bg-cover bg-center bg-no-repeat ">
        <div className="flex justify-center">
          <div className="w-[690px] bg-white shadow-2xl rounded-lg px-8 pt-6 pb-8 md:my-5">
            <div className="mb-7">
              <div className="py-4">
                <Link href="/">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    className="text-red-500 text-xl font-bold"
                    width="20"
                  />
                </Link>
              </div>
              <div className="text-2xl font-bold mb-5">
                <h1>Bagaimana cara masuk ke Aplikasi myIndiHome</h1>
              </div>
            </div>

            <section className="mb-7">
              <div className="mb-5">
                <p>
                  Buka Aplikasi myIndiHome Anda. Di bagian bawah, klik
                  &lsquo;masuk&lsquo; dengan menggunakan e-mail atau nomor
                  telepon. Berikut merupakan langkahnya:
                </p>
                <ol className="list-[lower-alpha] list-outside ml-4 m-3 text-justify">
                  <li className="mb-1">
                    Email: Anda dapat login dengan email apa pun yang terdaftar
                    di akun myIndiHome Anda
                  </li>
                  <li className="mb-1">
                    Nomor telepon: Jika Anda memiliki nomor ponsel yang
                    dikonfirmasi pada akun Anda, Anda dapat memasukkannya di
                    sini (Jika Anda menggunakan penyedia bahasa Indonesia,
                    silakan masukkan nomor ponsel Anda tanpa kode negara mis.
                    0812345678, jika Anda menggunakan penyedia lain, silakan
                    masukkan dengan kode negara nomor ponsel Anda, mis.
                    6512345678). Setelah Anda memasukkan email / nomor ponsel
                    Anda, klik &lsquo;Login&lsquo;
                  </li>
                  <li className="mb-1">
                    Jika Anda menggunakan nomor ponsel Anda untuk login, kode
                    verifikasi 4 digit akan dikirim setelah Anda memilih metode
                    verifikasi (antara Whatsapp atau SMS).
                  </li>
                  <li className="mb-1">
                    Jika Anda menggunakan email untuk masuk, masukkan kata sandi
                    Anda. Klik &lsquo;Selanjutnya&lsquo;.
                  </li>
                </ol>
              </div>
            </section>
          </div>
        </div>
      </div>
    </Layout>
  );
}
