import { useEffect, useState } from 'react';
import Layout from '../../../../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';

export default function PageServices() {
  const [category, setCategory] = useState('');

  useEffect(() => {
    setCategory(localStorage.getItem('category'));
  }, []);

  const gangguanCategory = [
    {
      name: 'Administrasi',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Permintaan Migrasi/merubah Paket',
        },
        {
          Qid: 2,
          title: 'Permintaan Rincian Tagihan',
        },
        {
          Qid: 3,
          title: 'Buka Status Isolir',
        },
      ],
    },
    {
      name: 'Telepon',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Tidak dapat melakukan panggilan telepon',
        },
        {
          Qid: 2,
          title: 'Telepon tidak dapat menerima panggilan',
        },
        {
          Qid: 3,
          title: 'Telepon tidak ada nada',
        },
      ],
    },
    {
      name: 'TV',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Konten di IndiHome TV tidak dapat tampil dilayar',
        },
        {
          Qid: 2,
          title: 'Layanan IndiHome TV tidak dapat berfungsi',
        },
        {
          Qid: 3,
          title: 'Remote IndiHome TV tidak berfungsi',
        },
      ],
    },
    {
      name: 'Internet',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Kecepatan internet sangat lambat',
        },
        {
          Qid: 2,
          title: 'Koneksi internet tidak stabil (putus-putus)',
        },
        {
          Qid: 3,
          title: 'ONT/ Modem tidak dapat berfungsi normal',
        },
      ],
    },
  ];

  return (
    <Layout>
      <div className="body-bg bg-white">
        <div className="flex justify-center">
          <div className="max-w-3xl bg-white shadow-2xl rounded-lg px-8 pt-6 pb-8 md:mb-4 ">
            <div className="mb-4">
              <div className="py-4">
                <Link href="/help">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    className="text-red-500 text-xl font-bold"
                    width="15"
                  />
                </Link>
              </div>
              <h1 className="text-3xl font-bold ">Pengaduan Layanan</h1>
            </div>

            <section>
              <div>
                <p>
                  Mohon maaf atas ketidaknyamanannya. Mohon laporkan masalah
                  atau gangguan tersebut di sini agar Kami dapat segera
                  menindaklanjutinya.
                </p>
                <div className="mt-8">
                  <label
                    htmlFor="gangguan"
                    className="block text-xs font-medium text-gray-500 uppercase tracking-widest mb-3 "
                  >
                    Gangguan yang dialami
                  </label>
                  <select
                    id="gangguan"
                    name="gangguan"
                    className="mt-1 block w-full p-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 focus:border-2 sm:text-sm"
                  >
                    <option disabled selected>
                      Pilih Kategori
                    </option>
                    {gangguanCategory.map(
                      (item) =>
                        item.name === category &&
                        item.pertanyaan.map((items) => (
                          <option value={items.title} key={items.Qid}>
                            {items.title}
                          </option>
                        ))
                    )}
                  </select>
                </div>
                <div className="mt-6">
                  <label
                    htmlFor="gangguan"
                    className="block text-xs font-medium text-gray-500 uppercase tracking-widest mb-3 "
                  >
                    Komentar/Masukan Anda
                  </label>
                  <textarea
                    id="gangguan"
                    name="gangguan"
                    rows="4"
                    placeholder="Masukkan komentar Anda disini"
                    className="mt-1 block w-full p-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 focus:border-2 sm:text-sm"
                  />
                </div>
                <button className="btn btn-primary text-white w-full py-3 mt-8 rounded-md">
                  Laporkan Gangguan
                </button>
              </div>
            </section>
          </div>
        </div>
      </div>
    </Layout>
  );
}
