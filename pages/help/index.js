import { useState, useEffect } from 'react';
import Layout from '../../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowLeft,
  faChevronRight,
  faMagnifyingGlass,
} from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import Image from 'next/image';
import PengaduanLayanan from '../../components/Modal/PengaduanLayanan';

export default function HelpPage({ data }) {
  console.log(data);
  const [changeTab, setChangeTab] = useState(1);

  useEffect(() => {
    document.body.className = 'body-bg';
  }, []);

  const topik = [
    {
      topikID: 1,
      title: 'Pertanyaan Yang Sering Diajukan',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Bagaimana cara saya berbagi koneksi WiFi dengan orang lain',
        },
        {
          Qid: 2,
          title: 'Fitur apa yang terdapat dengan IndiHome TV',
        },
        {
          Qid: 3,
          title: 'Layanan IndiHome TV tidak dapat berfungsi',
        },
        {
          Qid: 4,
          title: 'Tidak dapat melakukan panggilan telepon',
        },
      ],
    },
    {
      topikID: 2,
      title: 'Internet',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Permintaan Migrasi/ merubah paket',
        },
        {
          Qid: 2,
          title: 'Permintaan Rincian Tagihan',
        },
        {
          Qid: 3,
          title: 'Buka status Isolir',
        },
        {
          Qid: 4,
          title: 'Apa itu IndiHome Fiber?',
        },
      ],
    },
    {
      topikID: 3,
      title: 'Telepon',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Tidak dapat melakukan panggilan telepon',
        },
        {
          Qid: 2,
          title: 'Telepon tidak ada nada',
        },
        {
          Qid: 3,
          title: 'Telepon tidak dapat menerima panggilan',
        },
        {
          Qid: 4,
          title: 'Suara telepon tidak jernih/kemerosok',
        },
      ],
    },

    {
      topikID: 4,
      title: 'Administrasi dan Tagihan',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Kecepatan internet sangat lambat',
        },
        {
          Qid: 2,
          title: 'Koneksi internet tidak stabil (putus-putus)',
        },
        {
          Qid: 3,
          title: 'ONT/Modem tidak dapat berfungsi normal',
        },
        {
          Qid: 4,
          title: 'Bagaimana cara saya berbagi koneksi Wifi dengan orang lain',
        },
      ],
    },
    {
      topikID: 5,
      title: 'TV',
      pertanyaan: [
        {
          Qid: 1,
          title: 'Konten di IndiHome TV tidak tampil dilayar',
        },
        {
          Qid: 2,
          title: 'Fitur apa yang tedapat dengan IndiHome TV?',
        },
        {
          Qid: 3,
          title: 'Apa itu IndiHome TV Cable?',
        },
        {
          Qid: 4,
          title: 'Apa itu Minipack IndiHome TV?',
        },
      ],
    },
  ];

  return (
    <Layout>
      <section className="py-5">
        <div className="flex flex-col max-w-6xl px-4 mx-auto sm:px-6 lg:px-8">
          <div className="flex flex-col items-center justify-center text-center lg:text-left lg:items-start">
            <div className="py-4 w-full">
              <Link
                href="/"
                className="uppercase text-sm font-bold tracking-wide"
              >
                <div className="flex">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    width={15}
                    className="flex mr-1"
                  />
                  <p>Beranda</p>
                </div>
              </Link>
            </div>
            <div className="flex flex-row relative justify-between w-full">
              <div className="mt-2">
                <h1 className="text-4xl font-bold tracking-wide">Bantuan</h1>
                <p className="mt-2">
                  Anda mempunyai gangguan dan keluhan? Lakukan pengaduan
                  layanan! Tim Kami siap membantu Anda.
                </p>
                <PengaduanLayanan />
              </div>
              <div className="mt-2 absolute -top-8 right-4 md:static">
                <FontAwesomeIcon
                  icon={faMagnifyingGlass}
                  width={20}
                  className="text-xl"
                />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="pb-5 pt-12">
        <div className="flex flex-col max-w-6xl px-4 mx-auto sm:px-6 lg:px-8">
          <div className="flex flex-col lg:flex-row lg:text-left lg:items-start">
            <div className="flex flex-col pb-4">
              <p className="uppercase text-[.9rem] tracking-widest text-[#999]">
                Topik
              </p>
              <div className="flex flex-col">
                {topik.map((item) => (
                  <div key={item.topikID}>
                    <input
                      type="radio"
                      name="tabs"
                      id={`tab-${item.topikID}`}
                      className="hidden peer"
                      onChange={() => setChangeTab(item.topikID)}
                      checked={changeTab === item.topikID}
                    />
                    <label
                      htmlFor={`tab-${item.topikID}`}
                      className={`tracking-wider  peer-checked:text-white flex justify-between items-center bg-white rounded-lg mt-3 pl-4 pr-12 py-5 shadow-sm hover:shadow-lg ${
                        changeTab === 1
                          ? 'peer-checked:bg-black'
                          : 'peer-checked:bg-[url(/assets/img/bg_help.jpeg)]'
                      } cursor-pointer`}
                    >
                      {item.title}
                    </label>
                  </div>
                ))}
              </div>
              <div className="hidden lg:flex justify-center items-center location-banner text-white rounded-lg mt-3 p-3 pr-0">
                <div className="flex flex-col m-3">
                  <Image
                    src="/assets/img/logo_location.png"
                    width={75}
                    height={75}
                    alt="Location"
                  />
                </div>
                <div className="w-60 mr-3">
                  <p className="max-w-5xl">
                    Butuh bantuan? Kunjungi Plasa Telkom terdekat
                  </p>
                  <Link
                    href="https://my.indihome.co.id/nearest-plaza"
                    className="uppercase block font-semibold py-3 px-6 text-black bg-white rounded-lg transition duration-300 mt-2"
                  >
                    Cari Plasa Terdekat
                  </Link>
                </div>
              </div>
            </div>
            <div className="flex flex-col p-4 m-0 md:m-4 bg-white rounded md:w-[45rem]">
              {topik.map(
                (item) =>
                  item.topikID === changeTab && (
                    <div key={item.id}>
                      <p className="uppercase text-[.9rem] tracking-widest text-[#999]">
                        {item.title}
                      </p>
                      <div className="flex flex-col">
                        {item.pertanyaan.map((pertanyaan) => (
                          <Link
                            href={`/help/${pertanyaan.Qid}`}
                            key={pertanyaan.Qid}
                            className="border-b border-[#eee] last:border-b-0 py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                          >
                            <p className="font-bold text-lg">
                              {pertanyaan.title}
                            </p>
                            <FontAwesomeIcon
                              icon={faChevronRight}
                              width={8}
                              className="text-red-500 mr-3"
                            />
                          </Link>
                        ))}
                      </div>
                    </div>
                  )
              )}
            </div>
          </div>
          <div className="flex lg:hidden justify-center items-center location-banner text-white rounded-lg mt-3 p-3 pr-0">
            <div className="flex flex-col m-3">
              <Image
                src="/assets/img/logo_location.png"
                width={75}
                height={75}
                alt="Location"
              />
            </div>
            <div className="w-60 mr-3">
              <p className="max-w-5xl">
                Butuh bantuan? Kunjungi Plasa Telkom terdekat
              </p>
              <Link
                href="https://my.indihome.co.id/nearest-plaza"
                className="uppercase block font-semibold py-3 px-6 text-black bg-white rounded-lg transition duration-300 mt-2"
              >
                Cari Plasa Terdekat
              </Link>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}

export async function getServerSideProps() {
  const res = await fetch('http://localhost:6080/api/v1/bantuan');
  const data = await res.json();
  return {
    props: {
      data: data.data,
    },
  };
}
