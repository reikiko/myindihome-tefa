import { useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Layout from '../components/Layout';

export default function ChangeEmail() {
  useEffect(() => {
    document.body.className = 'body-bg';
  });
  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[500px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 md:mb-[3.55rem] sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <div className="flex justify-center mt-8">
              <div className="flex flex-col items-center">
                <Image
                  src="/assets/img/myindihome.png"
                  width={350}
                  height={350}
                  alt="MyIndiHome Logo"
                />
                <p>ver. 1.2.2179 (Web)</p>
                <p>ver. 4.2.5 (Android/iOS)</p>
                <p className="text-center mt-10">
                  myIndiHome adalah aplikasi yang digunakan untuk menemani Anda
                  dalam menikmati layanan IndiHome di mana pun dan kapan pun.
                  Nikmati kemudahan akses layanan dan fitur myIndiiHome mulai
                  dari aktivasi OTT, registrasi fitur tambahan, laporan gangguan
                  layanan, cek tagihan dan poin, serta berbagai fitur lainnya.
                </p>
                <div className="flex flex-row items-center space-x-8 mt-12 mb-4">
                  <a
                    href="https://facebook.com/indihome"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/assets/img/fb_logo.png"
                      width={60}
                      height={60}
                      alt="Facebook Logo"
                    />
                  </a>
                  <a
                    href="https://twitter.com/indihome"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/assets/img/twitter_logo.png"
                      width={60}
                      height={60}
                      alt="Twitter Logo"
                    />
                  </a>
                  <a
                    href="https://instagram.com/indihome"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/assets/img/ig_logo.png"
                      width={60}
                      height={60}
                      alt="Instagram Logo"
                    />
                  </a>
                  <a
                    href="https://indihome.com"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src="/assets/img/social_logo.png"
                      width={60}
                      height={60}
                      alt="Social Logo"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
