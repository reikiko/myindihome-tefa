import { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCircleQuestion,
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';
import InvalidModal from '../../components/Modal/InvalidLoginModal';

const Login = () => {
  const [email, setEmail] = useState('');
  const [phoneNum, setPhoneNum] = useState('');
  const [valueCondition, setValueCondition] = useState(true);
  const [failedCondition, setFailedCondition] = useState(false);

  const onCheck = async (e) => {
    e.preventDefault();
    if (email) {
      await fetch('http://localhost:8080/api/v1/users/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          value: email,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.code === 200) {
            window.location.href = '/login/password';
          } else {
            setFailedCondition(true);
          }
        });
    } else {
      await fetch('http://localhost:8080/api/v1/users/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          value: phoneNum,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.code === 200) {
            window.location.href = '/login/password';
          } else {
            setFailedCondition(true);
          }
        });
    }
  };
  const regexFirstSpace = (value) => {
    return value.charAt(0) === ' ';
  };

  useEffect(() => {
    localStorage.setItem('email', email);
    localStorage.setItem('phoneNum', phoneNum);
  }, [email, phoneNum]);

  return (
    <div
      className="bg-[url('/assets/img/bg_header.png')] bg-cover bg-center bg-no-repeat "
      data-testid="heading"
    >
      <div>
        <nav className="pt-9 pb-7 mb-7 hidden sm:block">
          <div className="flex justify-center">
            <Link href="/">
              <Image
                src="/assets/img/logo_indihome.png"
                width={148}
                height={148}
                alt="IndiHome"
              />
            </Link>
          </div>
        </nav>
        <div className="flex justify-center">
          <div className="md:w-[490px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem]">
            <div className="mb-7">
              <div className="flex justify-between">
                <Link href="/">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    className="text-red-500 text-lg"
                    width={20}
                  />
                </Link>
                <Link href="/help/internet/detail/1">
                  <FontAwesomeIcon
                    icon={faCircleQuestion}
                    className="text-red-500 text-lg"
                    width={20}
                  />
                </Link>
              </div>
              <Image
                src="/assets/img/header_login.png"
                width={102}
                height={102}
                alt="Register Logo"
                className="mb-4"
              />
              <h1 className="text-3xl font-bold text-[#353637]">
                Masuk ke myIndiHome
              </h1>
            </div>

            <form>
              <div className="mb-4">
                <label
                  className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                  htmlFor="email"
                >
                  Nomor Ponsel atau Email
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-sm"
                  id="email"
                  type="text"
                  placeholder="Masukkan nomor ponsel atau email"
                  onBlur={(e) => {
                    if (
                      e.target.value === '' ||
                      regexFirstSpace(e.target.value)
                    ) {
                      setValueCondition(false);
                    } else {
                      setValueCondition(true);
                    }
                    if (e.target.value.includes('@')) {
                      setEmail(e.target.value);
                    } else {
                      setPhoneNum(e.target.value);
                    }
                  }}
                />
                {!valueCondition ? (
                  <p className="text-red-500 text-xs pt-2">
                    Mohon cantumkan nomor ponsel atau email yang aktif
                  </p>
                ) : null}
              </div>
              <div className="flex justify-center mt-8">
                {email !== '' || phoneNum !== '' ? (
                  <button
                    className="bg-red-500 hover:bg-red-700 disabled:bg-red-500 py-3 rounded-lg focus:outline-none focus:shadow-outline w-full transition duration-100 ease-in-out"
                    onClick={onCheck}
                  >
                    <span
                      className={`uppercase font-bold text-sm ${
                        email !== '' || phoneNum !== ''
                          ? 'text-white'
                          : 'text-[#f69891]'
                      } flex justify-center`}
                    >
                      Masuk
                    </span>
                  </button>
                ) : (
                  <button
                    className="bg-red-500 hover:bg-red-700 disabled:bg-red-500 py-3 rounded-lg focus:outline-none focus:shadow-outline w-full transition duration-100 ease-in-out"
                    type="submit"
                    disabled={email !== '' || phoneNum !== '' ? false : true}
                  >
                    <span
                      className={`uppercase font-bold text-sm ${
                        email !== '' || phoneNum !== ''
                          ? 'text-white'
                          : 'text-[#f69891]'
                      }`}
                    >
                      Masuk
                    </span>
                  </button>
                )}
              </div>
            </form>
            <InvalidModal
              show={failedCondition}
              onClose={() => setFailedCondition(false)}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
