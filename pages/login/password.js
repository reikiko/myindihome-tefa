import { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCircleQuestion,
  faArrowLeft,
  faEyeSlash,
  faEye,
} from '@fortawesome/free-solid-svg-icons';

const Password = () => {
  const [email, setEmail] = useState('');
  const [phoneNum, setPhoneNum] = useState('');
  const [password, setPassword] = useState('');
  const [passwordCondition, setPasswordCondition] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    document.title =
      'IndiHome | New Excitement | Internet Rumah dan Cepat Hingga 300Mbps';
    setEmail(localStorage.getItem('email'));
    setPhoneNum(localStorage.getItem('phoneNum'));
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    if (email) {
      await fetch('http://localhost:8080/api/v1/users/login/password', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          value: email,
          password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.code === 200) {
            localStorage.setItem('token', data.data.jwt);
            window.location.href = '/';
          } else {
            setPasswordCondition(true);
          }
        });
    } else {
      await fetch('http://localhost:8080/api/v1/users/login/password', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          value: phoneNum,
          password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.code === 200) {
            localStorage.setItem('token', data.data.jwt);
            window.location.href = '/';
          } else {
            setPasswordCondition(true);
          }
        });
    }
  };

  return (
    <div className="body-bg bg-white" data-testid="heading">
      <div>
        <nav className="pt-9 pb-7 mb-7 hidden sm:block">
          <div className="flex justify-center">
            <Link href="/">
              <Image
                src="/assets/img/logo_indihome.png"
                width={148}
                height={148}
                alt="IndiHome"
              />
            </Link>
          </div>
        </nav>
        <div className="flex justify-center">
          <div className="md:w-[490px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem]">
            <div className="mb-7">
              <div className="flex justify-between">
                <Link href="/login">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    className="text-red-500 text-lg"
                    width={20}
                  />
                </Link>
                <Link href="/help/internet/detail/1">
                  <FontAwesomeIcon
                    icon={faCircleQuestion}
                    className="text-red-500 text-lg"
                    width={20}
                  />
                </Link>
              </div>
              <Image
                src="/assets/img/header_password.png"
                width={102}
                height={102}
                alt="Register Logo"
                className="mb-4"
              />
              <h1 className="text-3xl font-bold text-[#353637]">
                Masukkan Kata Sandi Anda
              </h1>
            </div>

            <form>
              <div className="mb-4">
                <label
                  className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                  htmlFor="password"
                >
                  Kata sandi
                </label>
                <div className="relative">
                  <input
                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-sm"
                    id="password"
                    type={showPassword ? 'text' : 'password'}
                    placeholder="Masukkan kata sandi Anda"
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  />
                  <div className="absolute right-0 top-0 mt-[.45rem]">
                    {showPassword ? (
                      <FontAwesomeIcon
                        icon={faEye}
                        width={22}
                        className="text-md text-red-500 cursor-pointer mr-2 mt-[0.11rem]"
                        onClick={() => setShowPassword(!showPassword)}
                      />
                    ) : (
                      <FontAwesomeIcon
                        icon={faEyeSlash}
                        className="text-md text-[#999] cursor-pointer mr-2 mt-[0.11rem]"
                        width={22}
                        onClick={() => setShowPassword(!showPassword)}
                      />
                    )}
                  </div>
                  {passwordCondition ? (
                    <span className="text-red-500 text-xs">
                      Kata sandi yang Anda masukkan salah
                    </span>
                  ) : (
                    ''
                  )}
                </div>
              </div>

              <div className="flex justify-center mt-8">
                <button
                  className="bg-red-500 hover:bg-red-700 disabled:bg-red-500 py-3 rounded-lg focus:outline-none focus:shadow-outline w-full transition duration-100 ease-in-out"
                  type="button"
                  disabled={password !== '' ? false : true}
                  onClick={onSubmit}
                >
                  <span
                    className={`uppercase font-bold text-sm ${
                      password !== '' ? 'text-white' : 'text-[#f69891]'
                    }`}
                  >
                    Next
                  </span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Password;
