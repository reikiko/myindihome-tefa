import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { useState } from 'react';
import Layout from '../components/Layout';

export default function SendFeedback() {
  const [topik, setTopik] = useState('');
  const [komentar, setKomentar] = useState('');
  return (
    <Layout>
      <div className="body-bg bg-white">
        <div className="flex justify-center">
          <div className="md:w-[35%] bg-white shadow-2xl rounded-lg px-8 pt-6 pb-8 md:mb-4">
            <div className="mb-4">
              <div className="py-4">
                <Link href="/profile">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    className="text-red-500 text-xl font-bold"
                    width="20"
                  />
                </Link>
              </div>
              <h1 className="text-3xl font-bold ">Kirim Masukkan Untuk Kami</h1>
            </div>

            <section>
              <div>
                <p>
                  Jika Anda memiliki kritik atau saran mengenai produk, layanan,
                  serta aplikasi. Kami dengan senang hati akan menerimanya
                </p>
                <div className="mt-8">
                  <label
                    htmlFor="gangguan"
                    className="block text-xs font-medium text-gray-500 uppercase tracking-widest mb-3 "
                  >
                    Topik
                  </label>
                  <select
                    id="gangguan"
                    name="gangguan"
                    className="mt-1 block w-full p-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 focus:border-2 sm:text-sm"
                  >
                    <option disabled selected>
                      Pilih Kategori
                    </option>
                    <option value="Suggestion">Suggestion</option>
                    <option value="Feedback">Feedback</option>
                  </select>
                </div>
                <div className="mt-6">
                  <label
                    htmlFor="gangguan"
                    className="block text-xs font-medium text-gray-500 uppercase tracking-widest mb-3 "
                  >
                    Komentar/Masukan Anda
                  </label>
                  <textarea
                    id="gangguan"
                    name="gangguan"
                    rows="4"
                    placeholder="Masukkan komentar Anda disini"
                    className="mt-1 block w-full p-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 focus:border-2 sm:text-sm"
                  />
                </div>
                <button
                  className="btn btn-primary text-white w-full py-3 mt-8 rounded-md"
                  disabled={topik === '' || komentar === ''}
                >
                  Laporkan Gangguan
                </button>
              </div>
            </section>
          </div>
        </div>
      </div>
    </Layout>
  );
}
