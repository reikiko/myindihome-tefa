import { useEffect, useState } from 'react';
import Layout from '../../components/Layout';
import Link from 'next/link';
import Image from 'next/image';
import Slider from '../../components/Slider/Slider';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import CardOffer from '../../components/Card/CardOffer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';

export default function Home() {
  const [name, setName] = useState('');
  const [isAuth, setIsAuth] = useState(false);
  const NotAuthPage = () => {
    return (
      <div className="mb-5 m-4">
        <div className="flex flex-wrap justify-between pr-6 sm:pr-4 md:ml-0 sm:ml-8 max-w-full md:max-w-xl">
          <div className="flex flex-col justify-center">
            <p className="text-sm text-white">Selamat Datang di</p>
            <p className="text-white font-bold text-[1.25rem]">myIndiHome X</p>
          </div>
          <div className="md:hidden block pt-5">
            <FontAwesomeIcon
              icon={faEnvelope}
              width={20}
              className="hover:text-red-500 transition duration-500 ease-in-out cursor-pointer"
            />
          </div>
        </div>
      </div>
    );
  };

  const AuthPage = () => {
    return (
      <div className="mb-5">
        <div className="flex flex-wrap justify-between pr-6 sm:pr-4 md:ml-4 sm:ml-8 max-w-full md:max-w-xl">
          <div className="flex flex-row">
            <div className="flex justify-start m-4">
              <div className="flex justify-center items-center w-16 h-16 bg-[#F2F2F2] border border-gray-300 rounded-full">
                <p className="text-2xl">{name[0]}</p>
              </div>
            </div>
            <div className="flex flex-col justify-center">
              <p className="text-sm text-[#ffffff80] uppercase font-bold">
                Selamat Datang
              </p>
              <p className="text-white font-bold text-[1.25rem]">
                {name} <span className="text-sm">&gt;</span>
              </p>
              <p className="text-white text-sm">0 Poin</p>
            </div>
          </div>
          <div className="md:hidden block pt-5">
            <FontAwesomeIcon
              icon={faEnvelope}
              width={20}
              className="hover:text-red-500 transition duration-500 ease-in-out cursor-pointer"
            />
          </div>
        </div>
      </div>
    );
  };

  const itemsLatest = [
    {
      image: 'latest_1worldcup.jpg',
      link: '/',
      alt: 'World Cup',
    },
    {
      image: 'latest_2smooa.jpg',
      link: '/',
      alt: 'Smooa',
    },
    {
      image: 'latest_3netflix.jpg',
      link: '/',
      alt: 'Netflix',
    },
    {
      image: 'latest_4indihome.jpg',
      link: '/',
      alt: 'Indihome',
    },
    {
      image: 'latest_5internet.jpg',
      link: '/',
      alt: 'Internet',
    },
    {
      image: 'latest_6internet2.jpg',
      link: '/',
      alt: 'Internet',
    },
    {
      image: 'latest_7suap.jpg',
      link: '/',
      alt: 'Suap',
    },
  ];

  const settings = {
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  };

  useEffect(() => {
    document.body.className =
      'bg-homebanner relative bg-no-repeat bg-cover bg-center w-full h-[400px] md:h-[684px]';
    const token = localStorage.getItem('token');
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const decodedData = JSON.parse(window.atob(base64));
      setName(decodedData.name);
      setIsAuth(true);
    }
  }, []);

  return (
    <Layout>
      <section className="relative">
        <div className="flex flex-col max-w-5xl mx-auto sm:mx-20 md:mx-14 lg:mx-16 xl:mx-[150px]">
          <div className="my-6">
            {isAuth ? <AuthPage /> : <NotAuthPage />}
            <div className="bg-white p-4 mx-auto ml-4 md:ml-4 rounded-lg max-w-md md:max-w-xl">
              <div className="flex flex-row">
                <div>
                  <h3 className="text-[1.575rem] font-bold text-gray-800 leading-tight">
                    Ayo langganan dan rasakan serunya internetan dengan IndiHome
                  </h3>
                  <p className="text-md text-gray-400 text-sm pt-3">
                    Kini internet cepat tanpa batasan mulai dari {''}
                    <span className="text-green-600 font-bold">
                      Rp275.000/bulan
                    </span>
                  </p>
                </div>
                <div className="flex justify-center items-center">
                  <Image
                    src="/assets/img/icon_internet.png"
                    height={80}
                    width={128}
                    alt="Internet"
                    className="max-w-full max-h-full"
                  />
                </div>
              </div>
              <div className="flex justify-center mt-10 mb-2">
                <Link
                  href="/shop/internet"
                  className="btn btn-primary text-white rounded-lg px-4 py-3 w-full uppercase font-bold text-sm"
                >
                  Cari Paket Internet
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className=" max-w-6xl px-4 mx-auto lg:flex-row sm:px-6 lg:px-8 mt-10 md:mt-24">
          <p className="uppercase text-sm text-[#999999]">Paling Populer</p>
        </div>
        <div className="max-w-6xl h-max mx-auto sm:px-6 lg:px-8 mt-10">
          <Slider {...settings}>
            {itemsLatest.map((item, index) => (
              <CardOffer
                key={index}
                title={item.title}
                image={item.image}
                link={item.link}
              />
            ))}
          </Slider>
        </div>
      </section>

      <section className="py-10 overflow-hidden">
        <div className=" max-w-6xl px-4 mx-auto lg:flex-row sm:px-6 lg:px-8">
          <p className="uppercase text-sm text-[#999999] mb-8">
            Penawaran Terbaru
          </p>
        </div>
        <Slider category="offers" className="" />
      </section>

      <section className="py-16 px-4 md:px-0 mx-auto bg-white">
        <div className="flex max-w-max mx-auto sm:ml-20 md:ml-14 lg:mx-16 xl:mx-[150px] h-max">
          <div className="mx-auto lg:flex-row lg:space-between sm:px-6 lg:px-8">
            <p className="uppercase text-sm text-[#999999] mb-8 tracking-widest">
              Streaming Sekarang di Netflix
            </p>
            <div className="md:block hidden">
              <Image
                src="/assets/img/netflix.png"
                width={80}
                height={80}
                alt="Netflix"
                className="mb-4"
              />
              <h3 className="text-3xl font-bold text-gray-800 mb-4">
                Langganan sekarang untuk streaming semua kontennya!
              </h3>
              <p className="text-gray-500 text-md">
                Rasakan serunya streaming film, drama series, TV series hingga
                Netflix Original.
              </p>
              <button className="btn btn-primary text-white rounded-lg px-4 py-3 w-80 uppercase font-bold text-sm mt-8">
                Langganan Sekarang
              </button>
            </div>
            <div className="md:hidden flex flex-row space-x-3">
              <Image
                className="rounded-xl shadow-md md:h-full"
                src="/assets/img/netflix_banner1.jpg"
                width={257}
                height={364}
                alt="Thirty-Nine"
              />
              <Image
                className="rounded-xl shadow-md md:h-full"
                src="/assets/img/netflix_banner2.jpg"
                width={257}
                height={364}
                alt="Forecasting Love & Weather"
              />
            </div>
          </div>
          <div className="hidden md:flex md:flex-row space-x-3 overflow-hidden">
            <Image
              className="rounded-xl shadow-md md:h-full"
              src="/assets/img/netflix_banner1.jpg"
              width={257}
              height={364}
              alt="Thirty-Nine"
            />
            <Image
              className="rounded-xl shadow-md md:h-full"
              src="/assets/img/netflix_banner2.jpg"
              width={257}
              height={364}
              alt="Forecasting Love & Weather"
            />
          </div>
        </div>
      </section>
    </Layout>
  );
}
