import { render, screen } from '@testing-library/react';
import LoginModal from '../../components/Modal/LoginModal';
import '@testing-library/jest-dom';

describe('Login Modal', () => {
  it('renders include a text', () => {
    render(<LoginModal />);
    expect(screen.getByText('Masuk/Daftar')).toBeInTheDocument();
  });
});
