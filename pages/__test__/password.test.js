import { render, screen } from '@testing-library/react';
import Password from '../login/password';
import '@testing-library/jest-dom';

describe('Password', () => {
  it('renders a heading', () => {
    render(<Password />);
    expect(screen.getByTestId('heading')).toBeInTheDocument();
  });
});
