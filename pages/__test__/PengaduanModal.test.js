import { render, screen } from '@testing-library/react';
import PengaduanLayanan from '../../components/Modal/PengaduanLayanan';
import '@testing-library/jest-dom';

describe('Login Modal', () => {
  it('renders include a text', () => {
    render(<PengaduanLayanan />);
    expect(screen.getByText('Pengaduan layanan')).toBeInTheDocument();
  });
});
