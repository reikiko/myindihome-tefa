import { render, screen } from '@testing-library/react';
import Login from '../login';
import '@testing-library/jest-dom';

describe('Login', () => {
  it('renders a heading', () => {
    render(<Login />);
    expect(screen.getByTestId('heading')).toBeInTheDocument();
  });
});
