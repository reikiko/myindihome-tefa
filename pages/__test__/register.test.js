import { render, screen } from '@testing-library/react';
import Register from '../register/account-details';
import '@testing-library/jest-dom';

describe('Register', () => {
  it('renders a heading', () => {
    render(<Register />);
    expect(screen.getByTestId('heading')).toBeInTheDocument();
  });
});
