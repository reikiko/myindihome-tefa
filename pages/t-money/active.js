import { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowLeft,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import Layout from '../../components/Layout';

export default function TMoney() {
  const [showPassword, setShowPassword] = useState(false);
  return (
    <Layout>
      <div className="flex justify-center ">
        <div className="md:w-[490px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 md:mb-[3.55rem] sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4 body-bg">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <Image
              src="/assets/img/header_wallet.png"
              width={102}
              height={102}
              alt="Wallet Logo"
              className="mb-4"
            />

            <h1 className="text-3xl font-bold text-[#353637]">
              Aktifkan Saldo
            </h1>
            <p>Buat kata sandi baru untuk mengaktifkan Saldo</p>
          </div>

          <form>
            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                htmlFor="sandi"
              >
                Kata sandi baru
              </label>
              <div className="relative">
                <input
                  className="shadow appearance-none border rounded-lg border-gray-300 w-full p-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="passwordLama"
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Masukkan kata sandi baru disini"
                />
                <div className="absolute right-0 top-0 mt-[0.65rem]">
                  {showPassword ? (
                    <FontAwesomeIcon
                      icon={faEye}
                      width={22}
                      className="text-md text-red-500 cursor-pointer mr-2 mt-[0.11rem]"
                      onClick={() => setShowPassword(!showPassword)}
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faEyeSlash}
                      className="text-md text-[#999] cursor-pointer mr-2 mt-[0.11rem]"
                      width={22}
                      onClick={() => setShowPassword(!showPassword)}
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="flex justify-center mt-8">
              <button
                className="bg-red-500 hover:bg-red-700 text-white text-sm font-bold p-3 rounded-lg w-full uppercase"
                type="submit"
              >
                Lanjut
              </button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
}
