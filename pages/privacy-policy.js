import Link from 'next/link';
import Layout from '../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
export default function EditPage() {
  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[60rem] sm:max-w-full bg-white shadow-2xl rounded-lg px-5 pt-6 pb-8 mb-24 sm:mx-4 body-bg-mobile sm:max-h-[50rem] mt-4">
          <div className="">
            <div className="flex justify-between">
              <Link href="/">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <h1 className="text-3xl font-bold text-[#353637] m-1">
              Kebijakan Privasi
            </h1>
            <div className="m-4">
              <ol className="list-decimal ml-7">
                <li>
                  Anda menyatakan dan menjamin bahwa Anda adalah individu yang
                  secara hukum berhak untuk mengadakan perjanjian yang mengikat
                  berdasarkan hukum Negara Republik Indonesia, khususnya
                  Ketentuan Penggunaan, untuk menggunakan Aplikasi dan bahwa
                  Anda telah berusia minimal 17 tahun atau sudah menikah dan
                  tidak berada di bawah perwalian. Jika tidak, Kami atau
                  penyedia Layanan terkait, berhak berdasarkan hukum untuk
                  membatalkan perjanjian yang dibuat dengan Anda. Anda
                  selanjutnya menyatakan dan menjamin bahwa Anda memiliki hak,
                  wewenang dan kapasitas untuk menggunakan Layanan dan mematuhi
                  Ketentuan Penggunaan. Jika Anda mendaftarkan atas nama suatu
                  badan hukum, Anda juga menyatakan dan menjamin bahwa Anda
                  berwenang untuk mengadakan, dan mengikatkan diri entitas
                  tersebut pada Ketentuan Penggunaan ini dan mendaftarkan untuk
                  Layanan dan Aplikasi.
                </li>
                <li>
                  Kami mengumpulkan dan memproses informasi pribadi Anda,
                  seperti nama, alamat surat elektronik (surel / e-mail), dan
                  nomor telepon seluler Anda ketika Anda mendaftar. Anda harus
                  memberikan informasi yang akurat dan lengkap, memperbarui
                  informasi dan setuju untuk memberikan kepada Kami perihal
                  bukti identitas apapun yang secara wajar dapat Kami mintakan.
                  Jika informasi pribadi yang Anda berikan kepada Kami diubah,
                  misalnya, jika Anda mengubah alamat surel, nomor telepon, atau
                  jika Anda ingin membatalkan akun Anda, mohon untuk
                  memperbaharui rincian informasi Anda dengan mengirimkan
                  permintaan Anda kepada Kami. Kami akan, sepanjang yang dapat
                  Kami lakukan, segera memberlakukan perubahan yang diminta
                  tersebut.
                </li>
                <li>
                  Anda hanya dapat menggunakan Aplikasi ketika Anda telah
                  mendaftar pada Aplikasi tersebut. Setelah Anda berhasil
                  mendaftarkan diri, Aplikasi akan memberikan Anda suatu akun
                  pribadi yang dapat diakses dengan kata sandi yang Anda pilih.
                </li>
                <li>
                  Kami berhak menolak untuk menerima permintaan Anda jika Kami
                  memiliki alasan yang wajar untuk mencurigai bahwa Anda telah,
                  atau dengan menerima permintaan dari Anda, Anda akan melanggar
                  Ketentuan Penggunaan ini atau hukum dan peraturan
                  perundang-undangan yang berlaku.
                </li>
                <li>
                  Kami dapat, berdasarkan kebijakan Kami, memberikan
                  promosi-promosi yang dapat ditukar untuk manfaat Anda terkait
                  dengan penggunaan Aplikasi. Anda setuju bahwa Anda hanya akan
                  menggunakan promosi tersebut sebagaimana promosi tersebut
                  dimaksudkan dan tidak akan menyalahgunakan, menggandakan,
                  menjual atau mengalihkan promosi tersebut dengan cara apapun.
                  Anda juga memahami bahwa promosi tidak dapat ditukarkan dengan
                  uang tunai dan dapat berakhir pada tanggal tertentu, bahkan
                  sebelum Anda menggunakannya.
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
