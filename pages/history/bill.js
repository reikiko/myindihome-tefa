import { useEffect, useState } from 'react';
import Link from 'next/link';
import Layout from '../../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image';

export default function BillPage() {
  useEffect(() => {
    document.body.className = '';
  }, []);
  const [changeTab, setChangeTab] = useState(1);
  const category = [
    {
      id: 1,
      title: 'Laporan Tagihan',
      content: [
        {
          subtitle: 'Pembayaran Anda',
          image: '/assets/img/blank_history.png',
          desc: 'Anda belum memiliki riwayat tagihan layanan. Segera langganan dan rasakan serunya seluruh layanan IndiHome!',
          isButton: true,
        },
      ],
    },
    {
      id: 2,
      title: 'Pembelian',
      content: [
        {
          subtitle: '',
          image: '/assets/img/blank_default.svg',
          desc: 'Anda belum memiliki riwayat transaksi. Segera langganan dan rasakan serunya seluruh layanan IndiHome',
          isButton: true,
        },
      ],
    },
    {
      id: 3,
      title: 'Aktivitas Layanan',
      content: [
        {
          subtitle: '',
          image: '/assets/img/blank_default.svg',
          desc: 'Anda tidak memiliki riwayat aktivitas layanan. Mulai langganan, tambah add-on atau layanan seru lain!',
          isButton: false,
        },
      ],
    },
  ];

  return (
    <Layout>
      <section className="py-5 banner internet">
        <div className="flex flex-col px-6 max-w-5xl mx-auto sm:mx-20 md:mx-14 lg:mx-16 xl:mx-[150px]">
          <div className="flex flex-col items-start justify-center text-center lg:text-left">
            <div className="py-4 text-[#e7c6c3] hover:text-white transition duration-500 ease-in-out">
              <Link
                href="/"
                className="uppercase text-sm font-bold tracking-wide"
              >
                <div className="flex">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    width={15}
                    className="flex mr-1"
                  />
                  <p>Beranda</p>
                </div>
              </Link>
            </div>
            <div className="flex flex-row justify-between w-full">
              <div className="mt-2">
                <h1 className="text-4xl text-white font-bold tracking-wide mt-14">
                  Riwayat
                </h1>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="pb-5 relative -top-10">
        <div className="flex flex-col max-w-6xl px-4 mx-auto">
          <div className="flex flex-col md:flex-row lg:text-left lg:items-start">
            <div className="flex flex-col">
              <div className="flex flex-col p-4 m-4 bg-white shadow-lg rounded-lg w-[350px]">
                <div className="flex flex-col">
                  {category.map((item, idx) => (
                    <div key={item.id}>
                      <input
                        type="radio"
                        name="tabs"
                        id={`tab-${item.id}`}
                        className="hidden peer"
                        onChange={() => setChangeTab(item.id)}
                        checked={changeTab === item.id}
                      />
                      <label
                        htmlFor={`tab-${item.id}`}
                        className={`tracking-wider peer-checked:text-red-500 flex justify-between items-center pl-4 pr-5 py-5 cursor-pointer font-bold hover:text-red-500 text-lg ${
                          idx === category.length - 1
                            ? ''
                            : 'border-b border-[#eee]'
                        } transition duration-500 ease-in-out`}
                      >
                        {item.title}
                        <FontAwesomeIcon
                          icon={faChevronRight}
                          width={10}
                          className="text-red-500 text-xs ml-16"
                        />
                      </label>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="flex flex-col p-4 m-4 bg-white rounded max-w-screen md:w-[45rem] shadow-lg">
              {category.map(
                (item) =>
                  item.id === changeTab && (
                    <div key={item.id}>
                      {item.content.map((content, idx) => (
                        <div key={idx} className="flex flex-col">
                          {content.subtitle !== '' && (
                            <p className="text-[#999] text-sm uppercase tracking-widest p-4">
                              {content.subtitle}
                            </p>
                          )}
                          <div className="flex flex-col items-center justify-center text-center mt-4">
                            <Image
                              src={content.image}
                              alt="blank"
                              width={100}
                              height={100}
                              className=""
                            />
                            <p className="max-w-screen lg:w-[500px] mt-4">
                              {content.desc}
                            </p>
                            {content.isButton && (
                              <button className="btn-primary uppercase text-white font-bold py-4 px-6 rounded-lg m-8 text-sm">
                                Cari Paket Internet
                              </button>
                            )}
                          </div>
                        </div>
                      ))}
                    </div>
                  )
              )}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
