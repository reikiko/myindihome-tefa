import Image from 'next/image';
import Link from 'next/link';
import Layout from '../../../components/Layout';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import CardPackage from '../../../components/Card/CardPackage';
import { useState } from 'react';

export default function Internet({ packageInternet }) {
  const [data, setData] = useState(packageInternet);
  const settings = {
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
    ],
  };
  return (
    <Layout>
      <section className="bg-bannerinternet relative bg-no-repeat bg-cover bg-center w-full h-[300px]">
        <div className="flex flex-col px-6 max-w-5xl mx-auto sm:mx-20 md:mx-14 lg:mx-16 xl:mx-[150px]">
          <ol className="flex py-5 w-full">
            <li className="text-white opacity-75 text-sm font-bold uppercase hover:opacity-100 duration-300">
              <Link href="/shop">
                <div className="flex flex-row gap-1 justify-center items-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-arrow-left"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                    />
                  </svg>
                  <span>Berlangganan</span>
                </div>
              </Link>
            </li>
          </ol>
          <div className="pt-12">
            <h1 className="text-[2.5rem] text-white font-bold">
              Produk Internet
            </h1>
            <p className="text-white opacity-75">
              Nikmati aneka paket internet dan layanan tambahan lainnya mulai
              dari Rp150.000 per bulan*!
            </p>
          </div>
        </div>
      </section>

      <section className="relative -top-8">
        <div className="mx-auto bg-white max-w-[1100px] rounded-none lg:rounded-md h-full shadow-lg ">
          <nav className="px-6 py-4 text-md">
            <Slider {...settings}>
              <li className="font-bold text-sm md:text-base max-w-fit">
                Paket
              </li>
              <li className="-ml-10 text-sm md:text-base">Upload Booster</li>
              <li className="-ml-10 text-sm md:text-base">Upgrade Speed</li>
              <li className="-ml-10 text-sm md:text-base">Speed On Demand</li>
              <li className="-ml-10 text-sm md:text-base">Wifi.id Seamless</li>
              <li className="-ml-10 text-sm md:text-base">Wifi Extender</li>
              <li className="-ml-10 text-sm md:text-base">Smooa</li>
              <li className="-ml-10 text-sm md:text-base">Top Up Quota</li>
            </Slider>
          </nav>
        </div>
      </section>

      <section className="py-10">
        <div className="flex space-between lg:max-w-6xl sm:max-w-xl mx-auto flex-row px-8 md:px-[15px] lg:mx-16 xl:mx-[156px]">
          <div className="flex flex-col justify-center text-left items-start md:justify-center">
            <Image
              src="/assets/img/indonesia.png"
              width={500}
              height={500}
              alt="Peta Indonesia"
              className="lg:block xl:hidden mx-auto"
            />
            <p className="hidden lg:block uppercase text-sm text-[#999999]">
              Dapatkan jangkauan luas layanan dan sinyal kuat dari IndiHome
            </p>
            <h1 className="my-4 text-2xl lg:text-3xl font-bold text-[#343536]">
              Layanan internet Kami siap menjangkau Anda dari Sabang sampai
              Merauke
            </h1>
          </div>
          <Image
            src="/assets/img/indonesia.png"
            width={500}
            height={500}
            alt="Peta Indonesia"
            className="xl:block hidden w-auto h-[100px] lg:h-auto flex-1"
          />
        </div>
      </section>

      <section className="pb-10">
        <div className="flex items-center justify-start md:justify-center px-8 mx-auto sm:mx-20 md:mx-14 lg:mx-16 xl:mx-[150px]">
          <h1 className="uppercase md:normal-case text-sm text-[#999999] md:mb-6 md:text-3xl font-light md:font-bold md:text-[#343536]">
            Paket Paling Populer
          </h1>
        </div>
        <div className="flex flex-col lg:grid lg:grid-cols-3 lg:items-start lg:justify-start items-center justify-center gap-4 gap-y-6 lg:gap-y-20 mx-auto my-10 px-8 md:px-[15px] md:mx-14 lg:mx-16 xl:mx-[156px]">
          <CardPackage data={data} />
        </div>
      </section>
    </Layout>
  );
}

export async function getServerSideProps() {
  const res = await fetch('http://localhost:7080/api/v1/product');
  const packageInternet = await res.json();
  return {
    props: {
      packageInternet: packageInternet.data,
    },
  };
}
