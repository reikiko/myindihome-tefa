import { useState, useEffect } from 'react';
import Layout from '../../../../components/Layout';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowLeft,
  faMagnifyingGlass,
} from '@fortawesome/free-solid-svg-icons';

export default function PasangBaruPage() {
  const [name, setName] = useState('');
  const [isAuth, setIsAuth] = useState(false);
  useEffect(() => {
    document.body.className = 'body-bg';
    const token = localStorage.getItem('token');
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const decodedData = JSON.parse(window.atob(base64));
      setName(decodedData.name);
      setIsAuth(true);
    }
  }, []);
  return (
    <Layout>
      <section className="py-5">
        <div className="flex flex-col max-w-6xl px-4 mx-auto sm:px-6 lg:px-8">
          <div className="flex flex-col items-center justify-center text-center lg:text-left lg:items-start">
            <div className="py-4 w-full">
              <Link
                href="/shop/internet"
                className="uppercase text-sm font-bold tracking-wide"
              >
                <div className="flex">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    width={15}
                    className="flex mr-1"
                  />
                  <p>Kembali</p>
                </div>
              </Link>
            </div>
            <div className="flex flex-row relative justify-between w-full">
              <div className="mt-2">
                <h1 className="text-4xl font-bold tracking-wide">
                  Alamat Pemasangan
                </h1>
              </div>
              <div className="mt-2 absolute -top-8 right-4 md:static">
                <FontAwesomeIcon
                  icon={faMagnifyingGlass}
                  width={20}
                  className="text-xl"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="px-4 mx-auto md:ml-36 sm:px-6 lg:px-8">
          <div className="flex flex-col p-6 my-8 bg-white rounded-md max-w-screen h-full md:w-4/5 shadow-lg ">
            <form>
              <div className="mb-6">
                <label
                  for="username"
                  className="block mb-2 text-sm font-medium text-gray-900"
                >
                  Username
                </label>
                <input
                  type="text"
                  id="first_name"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-red-500 focus:border-red-500 block w-full p-2.5"
                  placeholder={name}
                  disabled
                ></input>
              </div>
              <div className="grid gap-6 mb-6 lg:grid-cols-2">
                <div>
                  <label
                    for="province"
                    className="block mb-2 text-sm font-medium text-gray-900 "
                  >
                    Provinsi
                  </label>
                  <input
                    type="text"
                    id="company"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-red-500 focus:border-red-500 block w-full p-2.5"
                    placeholder=""
                    required
                  ></input>
                </div>
                <div>
                  <label
                    for="city"
                    className="block mb-2 text-sm font-medium text-gray-900 "
                  >
                    Kota
                  </label>
                  <input
                    type="text"
                    id="phone"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-red-500 focus:border-red-500 block w-full p-2.5"
                    required
                  ></input>
                </div>
                <div>
                  <label
                    for="address"
                    className="block mb-2 text-sm font-medium text-gray-900 "
                  >
                    Alamat Lengkap
                  </label>
                  <input
                    type="text"
                    id="website"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-red-500 focus:border-red-500 block w-full p-2.5"
                    placeholder=""
                    required
                  ></input>
                </div>
                <div>
                  <label
                    for="postal_code"
                    className="block mb-2 text-sm font-medium text-gray-900 "
                  >
                    Kode Pos
                  </label>
                  <input
                    type="text"
                    id="visitors"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-red-500 focus:border-red-500 block w-full p-2.5"
                    placeholder=""
                    required
                  ></input>
                </div>
              </div>

              <div className="flex items-start mb-6">
                <div className="flex items-center h-5">
                  <input
                    id="remember"
                    type="checkbox"
                    value=""
                    className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-red-300"
                    required
                  ></input>
                </div>
                <label
                  for="remember"
                  className="ml-2 text-sm font-medium text-gray-900 "
                >
                  I agree with the{' '}
                  <a href="#" className="text-red-500 hover:underline">
                    terms and conditions
                  </a>
                  .
                </label>
              </div>
              <button
                type="submit"
                className="text-white bg-red-600 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
              >
                Submit
              </button>
            </form>
          </div>
        </div>
      </section>
    </Layout>
  );
}
