import Image from 'next/image';
import Link from 'next/link';
import Layout from '../../components/Layout';
import CardOffer from '../../components/Card/CardOffer';
import Slider from '../../components/Slider/Slider';
import { useEffect } from 'react';

export default function Home() {
  useEffect(() => {
    document.body.className = '';
  });

  const itemsOffers = [
    {
      title: 'Cloud Storage',
      image: 'offers_1cloud_storage.png',
      link: '/',
    },
    {
      title: 'Smooa',
      image: 'offers_2smooa.png',
      link: '/',
    },
    {
      title: 'Pijar Belajar',
      image: 'offers_3pijar.png',
      link: '/',
    },
    {
      title: 'Benefit Voucher Game',
      image: 'offers_4benefitgame.png',
      link: '/',
    },
    {
      title: 'Upgrade Speed',
      image: 'offers_5upgrade_speed.png',
      link: '/',
    },
  ];

  return (
    <Layout>
      <section className="md:bg-banner relative bg-no-repeat bg-cover bg-center w-full max-h-[490px] md:h-[460px] overflow-hidden transistion">
        <div className="bg-mobilebanner bg-no-repeat bg-cover w-full h-[300px] z-40 md:hidden"></div>
        <div className="bg-waveredmobile bg-no-repeat bg-cover -top-[125px] w-full h-full flex relative items-center justify-center px-8 md:bg-none md:px-[15px] md:mx-14 md:top-0 md:justify-start lg:mx-16 xl:mx-[156px]">
          <div className="flex flex-col items-center justify-center max-w-full pt-28 pb-10 md:py-20 md:items-start md:justify-start md:max-w-xs lg:max-w-md text-white">
            <h3 className="text-center md:text-start font-bold text-2xl lg:text-3xl mb-4">
              Mau internetan makin cepat?
            </h3>
            <p className="banner-text">
              Upgrade kecepatan internet Anda untuk Rp10.000/bulan
            </p>
            <div className="bg-white w-fit py-3 px-5 rounded-md hover:bg-gray-100">
              <Link href="/">
                <p className="uppercase text-black text-sm font-bold">
                  Upgrade Paket Langganan
                </p>
              </Link>
            </div>
          </div>
        </div>
      </section>

      <section className="py-4 md:py-16 bg-white">
        <div className="flex flex-row gap-4 items-center justify-center text-center px-8 md:px-[15px] md:mx-14 lg:mx-16 xl:mx-[156px]">
          <Image
            className="rounded-lg w-[220px] md:w-max mb-4 shadow-xl hover:shadow-2xl"
            src="/assets/img/banner-ih.png"
            width={400}
            height={400}
            alt="shop"
          />
          <Image
            className="rounded-lg w-[220px] md:w-max mb-4 shadow-xl hover:shadow-2xl"
            src="/assets/img/rekomendasi-ih.webp"
            width={400}
            height={400}
            alt="shop"
          />
        </div>
      </section>

      <section className="py-4 md:py-16 bg-white">
        <div className="grid grid-cols-2 md:flex md:flex-row gap-4 items-center justify-center text-center px-8 md:px-[15px] md:mx-14 lg:mx-16 xl:mx-[156px]">
          <div className="max-w-max">
            <Link href="/shop/internet">
              <div className="card card-shop red">
                <div className="px-20 py-1">
                  <Image
                    className="max-w-max"
                    src="/assets/img/internet-icon.png"
                    width={75}
                    height={75}
                    alt="shop"
                  />
                </div>
                <span className="text-xl">Internet</span>
              </div>
            </Link>
          </div>
          <div className="max-w-max">
            <Link href="/shop/tv">
              <div className="card card-shop blue">
                <div className="px-20 py-1">
                  <Image
                    className="max-w-max"
                    src="/assets/img/tv-icon.png"
                    width={75}
                    height={75}
                    alt="shop"
                  />
                </div>
                <span className="text-xl">TV</span>
              </div>
            </Link>
          </div>
          <div className="max-w-max">
            <Link href="/shop/telepon">
              <div className="card card-shop green">
                <div className="px-20 py-1">
                  <Image
                    className="max-w-max"
                    src="/assets/img/telepon-icon.png"
                    width={75}
                    height={75}
                    alt="shop"
                  />
                </div>
                <span className="text-xl">Telepon</span>
              </div>
            </Link>
          </div>
          <div className="max-w-max">
            <Link href="/shop/others">
              <div className="card card-shop black">
                <div className="px-20 py-1">
                  <Image
                    className="max-w-max"
                    src="/assets/img/lainnya-icon.png"
                    width={75}
                    height={75}
                    alt="shop"
                  />
                </div>
                <span className="text-xl">Lainnya</span>
              </div>
            </Link>
          </div>
        </div>
      </section>

      <section className="py-20 overflow-hidden">
        <div className=" max-w-6xl px-4 mx-auto lg:flex-row sm:px-6 lg:px-8">
          <p className="uppercase text-sm text-[#999999] mb-8">
            Penawaran Terbaru
          </p>
        </div>
        <Slider category="offers" className="" />
      </section>
    </Layout>
  );
}
