import { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import CardSearch from '../components/Card/CardSearch';

export default function SearchPage({ packageInternet }) {
  const [searchResult, setSearchResult] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [data, setData] = useState(packageInternet);

  const handleSearch = (e) => {
    e.preventDefault();
    setSearchQuery(e.target.value);
    const result = data.filter((item) => {
      return item.name.toLowerCase().includes(searchQuery.toLowerCase());
    });
    setSearchResult(result);
  };

  return (
    <Layout>
      <div className="flex justify-center h-full mb-10">
        <div className="md:w-[60rem] sm:max-w-full bg-white shadow-2xl rounded-none md:rounded-lg px-5 pt-6 pb-8 mb-24 sm:mx-4 body-bg-mobile h-fit mt-0 md:mt-4">
          <div className="">
            <div className="flex justify-between">
              <Link href="/home">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <div className="flex justify-center">
              <div className="flex flex-col w-full">
                <div className="flex flex-col">
                  <input
                    type="text"
                    id="search"
                    className="w-full bg-white rounded-lg border border-gray-300 focus:border-gray-500 focus:ring-2 outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out mt-3"
                    onChange={handleSearch}
                  />
                </div>
              </div>
            </div>
          </div>
          {searchResult !== [] ? (
            <div className="mt-10 grid grid-cols-1 lg:grid-cols-3 gap-8">
              <CardSearch data={searchResult} />
            </div>
          ) : (
            ''
          )}
        </div>
      </div>
    </Layout>
  );
}

export async function getServerSideProps() {
  const res = await fetch('http://localhost:7080/api/v1/product');
  const packageInternet = await res.json();
  return {
    props: {
      packageInternet: packageInternet.data,
    },
  };
}
