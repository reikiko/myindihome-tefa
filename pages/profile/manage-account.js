import { useEffect } from 'react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import Layout from '../../components/Layout';
import Image from 'next/image';

export default function ManageAccount() {
  useEffect(() => {
    document.body.className = 'body-bg';
  });
  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[505px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 md:mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <div className="ml-1 mt-2">
              <h1 className="text-3xl font-bold text-[#353637] ">
                Mengelola Akun
              </h1>
            </div>
          </div>

          <div className="my-20">
            <div className="flex justify-center py-4">
              <div className="flex flex-col items-center">
                <Image
                  src="/assets/img/blank_default.svg"
                  width={200}
                  height={200}
                  alt="Blank"
                />
                <p className="text-center mt-2">
                  Anda tidak terhubung dengan akun IndiHome manapun. Klik tombol
                  di bawah untuk menghubungkan akun.
                </p>
              </div>
            </div>
          </div>

          <div className="flex justify-center mt-8">
            <button className="btn-primary text-white text-sm font-bold p-3 rounded-lg w-full uppercase">
              Hubungkan Nomor Indihome
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}
