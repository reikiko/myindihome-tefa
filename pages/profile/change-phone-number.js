import { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowLeft,
  faCircleQuestion,
} from '@fortawesome/free-solid-svg-icons';
import Layout from '../../components/Layout';

export default function ChangeNumber() {
  const [number, setNumber] = useState('');
  const handleNumberChange = (e) => {
    setNumber(e.target.value);
  };

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const decodedData = JSON.parse(window.atob(base64));
      setNumber(decodedData.notelp);
    } else {
      window.location.href = '/login';
    }
  }, []);

  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[490px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 md:mb-[3.55rem] sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={20}
                />
              </Link>
              <Link href="/help/internet/detail/1">
                <FontAwesomeIcon
                  icon={faCircleQuestion}
                  className="text-red-500 text-lg"
                  width={20}
                />
              </Link>
            </div>
            <Image
              src="/assets/img/header_verification.png"
              width={102}
              height={102}
              alt="Register Logo"
              className="mb-4"
            />
            <h1 className="text-3xl font-bold text-[#353637]">
              Ubah Nomor Ponsel
            </h1>
          </div>

          <form>
            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                htmlFor="ponsel"
              >
                Nomor Ponsel
              </label>
              <input
                className="shadow appearance-none border rounded-lg border-gray-300 w-full p-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="ponsel"
                type="number"
                value={number}
                onChange={handleNumberChange}
              />
            </div>
            <div className="flex justify-center mt-8">
              <button
                className="bg-red-500 hover:bg-red-700 text-white text-sm font-bold p-3 rounded-lg w-full uppercase"
                type="submit"
              >
                Lanjut
              </button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
}
