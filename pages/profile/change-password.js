import { useState } from 'react';
import {
  faArrowLeft,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import Layout from '../../components/Layout';

export default function ChangePassword() {
  const [showPasswordLama, setShowPasswordLama] = useState(false);
  const [showPasswordBaru, setShowPasswordBaru] = useState(false);
  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[490px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 md:mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>
            <div className="ml-1 mt-2">
              <h1 className="text-3xl font-bold text-[#353637] ">
                Ubah Kata Sandi
              </h1>
              <p className="text-[#999] mt-2">
                Masukkan kata sandi lama dan kata sandi baru Anda kehendaki
              </p>
            </div>
          </div>

          <form>
            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                htmlFor="passwordLama"
              >
                Kata Sandi Lama
              </label>
              <div className="relative">
                <input
                  className="shadow appearance-none border rounded-lg border-gray-300 w-full p-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="passwordLama"
                  type={showPasswordLama ? 'text' : 'password'}
                  placeholder="Masukkan kata sandi lama"
                />
                <div className="absolute right-0 top-0 mt-[0.65rem]">
                  {showPasswordLama ? (
                    <FontAwesomeIcon
                      icon={faEye}
                      width={22}
                      className="text-md text-red-500 cursor-pointer mr-2 mt-[0.11rem]"
                      onClick={() => setShowPasswordLama(!showPasswordLama)}
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faEyeSlash}
                      className="text-md text-[#999] cursor-pointer mr-2 mt-[0.11rem]"
                      width={22}
                      onClick={() => setShowPasswordLama(!showPasswordLama)}
                    />
                  )}
                </div>
              </div>
            </div>

            <div className="mb-12">
              <Link
                href="login/email/recovery-code"
                className="text-red-500 font-bold text-sm uppercase float-right"
              >
                Lupa Kata Sandi
              </Link>
            </div>

            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                htmlFor="passwordBaru"
              >
                Kata Sandi Baru
              </label>
              <div className="relative">
                <input
                  className="shadow appearance-none border rounded-lg border-gray-300 w-full p-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="passwordBaru"
                  type={showPasswordBaru ? 'text' : 'password'}
                  placeholder="Buat kata sandi baru"
                />
                <div className="absolute right-0 top-0 mt-[0.65rem]">
                  {showPasswordBaru ? (
                    <FontAwesomeIcon
                      icon={faEye}
                      width={22}
                      className="text-md text-red-500 cursor-pointer mr-2 mt-[0.11rem]"
                      onClick={() => setShowPasswordBaru(!showPasswordBaru)}
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faEyeSlash}
                      className="text-md text-[#999] cursor-pointer mr-2 mt-[0.11rem]"
                      width={22}
                      onClick={() => setShowPasswordBaru(!showPasswordBaru)}
                    />
                  )}
                </div>
              </div>
            </div>

            <div className="flex justify-center mt-8">
              <button
                className="bg-red-500 hover:bg-red-700 text-white text-sm font-bold p-3 rounded-lg w-full uppercase"
                type="submit"
              >
                Ubah Kata Sandi
              </button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
}
