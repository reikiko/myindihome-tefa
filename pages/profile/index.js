import { useEffect, useState } from 'react';
import Layout from '../../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image';
import toast from 'react-hot-toast';
import ModalMitra from '../../components/Modal/ComingSoonModal';
import ModalAktifkan from '../../components/Modal/AktifkanModal';
import ModalLogout from '../../components/Modal/LogoutModal';

export default function ProfilePage() {
  const [showModal, setShowModal] = useState(false);
  const [showModalAktifkan, setShowModalAktifkan] = useState(false);
  const [showModalLogout, setShowModalLogout] = useState(false);
  const [name, setName] = useState('');
  const [noHp, setNoHp] = useState('');

  const customToast = () => {
    toast.custom((t) => (
      <div className="bg-green-600 shadow-lg rounded-lg p-4 text-white">
        <p>Akun ini tidak terpaut no IndiHome!</p>
      </div>
    ));
  };

  useEffect(() => {
    document.body.className = 'bg-[#fafafa]';
    const token = localStorage.getItem('token');
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const decodedData = JSON.parse(window.atob(base64));
      setName(decodedData.name);
      setNoHp(decodedData.notelp);
    } else {
      window.location.href = '/login';
    }
  }, []);

  return (
    <Layout>
      <Image
        src="/assets/img/bg_profile.png"
        width={800}
        height={300}
        className="absolute top-[-18px]"
        alt="bg"
      />
      <section className="flex justify-center">
        <div className="flex flex-col justify-center relative lg:w-auto">
          <div>
            <div className="mt-12">
              <div className="mb-5">
                <div className="flex flex-row">
                  <div className="flex justify-start m-4 ">
                    <div className="flex justify-center items-center w-16 h-16 bg-[#F2F2F2] border border-gray-400 rounded-full">
                      <p className="text-2xl">{name[0]}</p>
                    </div>
                  </div>
                  <div className="flex flex-col justify-center">
                    <p className="font-bold text-[1.25rem]">{name}</p>
                    <p className="text-sm">{noHp}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-1 md:flex md:flex-row justify-between gap-3">
            <div className="bg-white p-4 rounded-lg w-[21.5rem] shadow-lg hover:shadow-2xl transition duration-500 ease-in-out">
              <Link
                href="/t-money/active"
                className="flex flex-row items-center "
              >
                <Image
                  src="/assets/img/wallet.png"
                  width={60}
                  height={60}
                  alt="wallet"
                />
                <p className="text-lg font-bold pr-16 m-4">Aktifkan Saldo</p>
                <FontAwesomeIcon
                  icon={faChevronRight}
                  width={8}
                  className="text-red-500 mr-3"
                />
              </Link>
            </div>
            <div className="bg-white p-4 rounded-lg w-[21.5rem] shadow-lg hover:shadow-2xl transition duration-500 ease-in-out">
              <button
                className="flex flex-row items-center"
                onClick={() => setShowModalAktifkan(true)}
              >
                <Image
                  src="/assets/img/gift.png"
                  width={60}
                  height={60}
                  alt="gift"
                />
                <p className="text-lg font-bold pr-12 m-4">Aktifkan Hadiah</p>
                <FontAwesomeIcon
                  icon={faChevronRight}
                  width={8}
                  className="text-red-500 mr-3"
                />
              </button>
              <ModalAktifkan
                show={showModalAktifkan}
                onClose={() => setShowModalAktifkan(false)}
              />
            </div>
          </div>
          <div className="bg-white shadow-2xl rounded-lg px-7 pt-6 pb-8 mb-24 mt-8">
            <div>
              <p className="text-gray-500 uppercase text-sm mb-4">
                Pengaturan Akun
              </p>

              <div className="flex flex-col">
                <Link
                  href="/profile/edit"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Ubah Profile Saya</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/profile/change-password"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Ubah Kata Sandi</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/profile/change-phone-number"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Ubah Nomor Ponsel</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/profile/change-email"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Ubah Email</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <button
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                  onClick={customToast}
                >
                  <p className="font-bold text-lg">Atur Pengguna</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </button>
                <Link
                  href="/profile/manage-account"
                  className="border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Atur Nomor Indihome</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
              </div>
            </div>

            <div>
              <p className="text-gray-500 uppercase text-sm my-4">
                Keanggotaan
              </p>

              <div className="flex flex-col">
                <Link
                  href="/help"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Tentang Poin myIndiHome</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <div className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200 cursor-pointer">
                  <p className="font-bold text-lg text-[#999]">
                    Lihat Penawaran Hadiah
                  </p>
                </div>
                <button
                  className="py-4 flex justify-between items-center hover:text-red-500 transition duration-200 cursor-pointer"
                  onClick={() => setShowModal(true)}
                >
                  <p className="font-bold text-lg">Penawaran dan Mitra</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </button>
                <ModalMitra
                  show={showModal}
                  onClose={() => setShowModal(false)}
                />
              </div>
            </div>

            <div>
              <p className="text-gray-500 uppercase text-sm my-4">Lainnya</p>

              <div className="flex flex-col">
                <Link
                  href="/t-money/active"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Pengaturan Saldo</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/manage-subscriptions"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">
                    Pengaturan Paket Langganan
                  </p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/notification-settings"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Pengaturan Notifikasi</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/send-feedback"
                  className="border-b border-[#eee] py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <p className="font-bold text-lg">Kirim Masukan Untuk Kami</p>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
                <Link
                  href="/about"
                  className="py-4 flex justify-between items-center hover:text-red-500 transition duration-200"
                >
                  <div>
                    <p className="font-bold text-lg">Tentang myIndiHome</p>
                    <span className="text-gray-500 text-sm">ver. 1.2.2176</span>
                  </div>
                  <FontAwesomeIcon
                    icon={faChevronRight}
                    width={8}
                    className="text-red-500 mr-3"
                  />
                </Link>
              </div>
            </div>
            <button
              className="btn btn-primary text-sm font-bold py-3 rounded-lg mt-8 w-full uppercase"
              onClick={() => setShowModalLogout(true)}
            >
              Keluar
            </button>
            <ModalLogout
              show={showModalLogout}
              onClose={() => setShowModalLogout(false)}
            />
          </div>
        </div>
      </section>
    </Layout>
  );
}
