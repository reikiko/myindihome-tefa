import { useState, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Layout from '../../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faCamera } from '@fortawesome/free-solid-svg-icons';
export default function EditPage() {
  const dateToday = new Date().toISOString().slice(0, 10);
  const [name, setName] = useState('');
  const [date, setDate] = useState(dateToday);
  const [email, setEmail] = useState('');
  const [gender, setGender] = useState('');

  const handleNameChange = (e) => {
    setName(e.target.value);
  };
  const handleDateChange = (e) => {
    setDate(e.target.value);
  };

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const decodedData = JSON.parse(window.atob(base64));
      setName(decodedData.name);
    }
  }, []);

  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[720px] sm:max-w-full bg-white shadow-2xl rounded-lg px-5 pt-6 pb-8 md:my-5 lg:mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem] lg:mt-4 ">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <h1 className="text-3xl font-bold text-[#353637] m-1">
              Ubah Profil Saya
            </h1>
            <div className="mt-4">
              <div className="relative flex justify-center items-center w-11 h-11 bg-[#F2F2F2] border border-gray-300 rounded-full">
                <p className="text-lg">{name[0]}</p>
                <input
                  type="file"
                  id="file"
                  className="hidden"
                  onChange={(e) => {
                    console.log(e.target.files[0]);
                  }}
                />
                <label htmlFor="file">
                  <FontAwesomeIcon
                    icon={faCamera}
                    className="text-red-500 text-lg cursor-pointer absolute "
                    width={15}
                  />
                </label>
              </div>
            </div>
          </div>

          <form>
            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                htmlFor="nama"
              >
                Nama Lengkap
              </label>
              <input
                className="shadow appearance-none border rounded-lg w-full p-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-300"
                id="nama"
                type="text"
                value={name}
                onChange={handleNameChange}
              />
            </div>

            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                htmlFor="nama"
              >
                Alamat
              </label>
              <textarea
                className="shadow appearance-none border rounded-lg w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline bg-[#e5e5e5] border-gray-300"
                placeholder="Alamat"
                disabled
              ></textarea>
            </div>
            <div className="flex md:flex-row md:justify-between gender-mobile">
              <div className="mb-4 w-80">
                <label
                  className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                  htmlFor="birthdate"
                >
                  Tanggal Lahir
                </label>
                <input
                  className="shadow appearance-none border rounded-lg w-full p-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-300"
                  id="birthdate"
                  type="date"
                  value={date}
                  onChange={handleDateChange}
                />
              </div>

              <div className="mb-4 w-1/2 ml-0 md:ml-4">
                <label
                  className="block text-[#999] text-xs font-normal tracking-widest mb-2 uppercase"
                  htmlFor="gender"
                >
                  Jenis Kelamin
                </label>
                <ul className="grid gap-3 w-full grid-cols-2">
                  <li>
                    <input
                      type="radio"
                      id="pria"
                      name="gender"
                      value="Laki-laki"
                      className="hidden peer"
                      required
                      onChange={(e) => setGender(e.target.value)}
                      checked
                    />
                    <label
                      htmlFor="pria"
                      className="inline-flex justify-between items-center p-3 w-full rounded-lg border cursor-pointer border-gray-300  peer-checked:border-[#0eb37e] peer-checked:bg-[#0eb37e] peer-checked:text-white hover:shadow-xl transition duration-300 ease-in-out"
                    >
                      <div className="block">
                        <div className="w-full">Laki-laki</div>
                      </div>
                    </label>
                  </li>
                  <li>
                    <input
                      type="radio"
                      id="perempuan"
                      name="gender"
                      value="Perempuan"
                      className="hidden peer"
                      required
                      onChange={(e) => setGender(e.target.value)}
                    />
                    <label
                      htmlFor="perempuan"
                      className="inline-flex justify-between items-center p-3 w-full rounded-lg border cursor-pointer border-gray-300  peer-checked:border-[#0eb37e] peer-checked:bg-[#0eb37e] peer-checked:text-white hover:shadow-xl transition duration-300 ease-in-out"
                    >
                      <div className="block">
                        <div className="w-full">Perempuan</div>
                      </div>
                    </label>
                  </li>
                </ul>
              </div>
            </div>

            <div className="flex justify-center mt-8">
              {email !== '' ? (
                <button className="bg-red-500 hover:bg-red-700 disabled:bg-red-500 py-3 rounded-lg focus:outline-none focus:shadow-outline w-full transition duration-100 ease-in-out">
                  <span
                    className={`uppercase font-bold text-sm ${
                      email !== '' ? 'text-white' : 'text-[#f69891]'
                    } flex justify-center`}
                  >
                    Simpan
                  </span>
                </button>
              ) : (
                <button
                  className="bg-red-500 hover:bg-red-700 disabled:bg-red-500 py-3 rounded-lg focus:outline-none focus:shadow-outline w-full transition duration-100 ease-in-out"
                  type="submit"
                  disabled={email !== '' ? false : true}
                >
                  <span
                    className={`uppercase font-bold text-sm ${
                      email !== '' ? 'text-white' : 'text-[#f69891]'
                    }`}
                  >
                    Simpan
                  </span>
                </button>
              )}
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
}
