import Image from 'next/image';
import Layout from '../components/Layout';
import Slider from '../components/Slider/Slider';
import { useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

export default function Home() {
  useEffect(() => {
    document.body.className = '';
  });

  return (
    <Layout>
      <div className="md:bg-banner relative bg-no-repeat bg-cover bg-center w-full max-h-[575px] md:h-full overflow-hidden transistion">
        <div className="bg-mobilebanner bg-no-repeat bg-cover w-full h-[300px] z-40 md:hidden"></div>
        <div className="bg-waveredmobile bg-no-repeat bg-cover -top-[125px] w-full h-full flex relative items-center justify-center px-8 md:bg-none md:px-[15px] md:mx-14 md:top-0 md:justify-start lg:mx-16 xl:mx-[156px]">
          <div className="flex flex-col items-center justify-center max-w-full pt-28 pb-10 md:py-20 md:items-start md:justify-start md:max-w-xs lg:max-w-md text-white">
            <h3 className="text-center md:text-start font-bold text-2xl lg:text-3xl mb-4">
              Selamat datang di IndiHome!
            </h3>
            <h3 className="font-bold text-2xl lg:text-3xl mb-4">
              Kami adalah penyedia layanan digital dan internet terlengkap di
              Indonesia
            </h3>
            <p className="banner-text text-xs leading-normal lg:text-xl mb-5">
              Nikmati paket langganan internet, TV, dan telepon mulai dari
              Rp150.000 per bulan*
            </p>
            <div className="bg-white w-fit mb-2 py-3 px-5 rounded-md hover:bg-gray-100">
              <Link href="/shop/internet">
                <p className="uppercase text-black text-sm font-bold">
                  Cari Paket Langganan
                </p>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <section className="py-10">
        <div className="flex space-between lg:max-w-6xl sm:max-w-xl mx-auto flex-row px-8 md:px-[15px] md:mx-14 lg:mx-16 xl:mx-[156px]">
          <div className="flex flex-col justify-center text-left items-start md:justify-center">
            <Image
              src="/assets/img/indonesia.png"
              width={500}
              height={500}
              alt="Peta Indonesia"
              className="lg:block xl:hidden mx-auto"
            />
            <p className="uppercase text-sm text-[#999999]">
              Dapatkan jangkauan luas layanan dan sinyal kuat dari IndiHome
            </p>
            <h1 className="my-4 text-2xl lg:text-3xl font-bold text-[#343536]">
              Layanan internet Kami siap menjangkau Anda dari Sabang sampai
              Merauke
            </h1>
            <button className="text-sm uppercase block font-semibold py-3 px-6 text-white hover:text-white bg-[#EE3225] rounded-lg transition duration-300">
              Cek Ketersediaan Paket
            </button>
          </div>
          <Image
            src="/assets/img/indonesia.png"
            width={500}
            height={500}
            alt="Peta Indonesia"
            className="xl:block hidden w-auto h-[100px] lg:h-auto flex-1"
          />
        </div>
      </section>

      <section className="md:bg-banner2 relative bg-no-repeat bg-cover bg-center w-full h-[500px]">
        <div className="bg-mobilebanner2 bg-no-repeat bg-cover w-full h-[300px] z-40 md:hidden"></div>
        <div className="bg-waveredmobile bg-no-repeat bg-cover -top-[145px] relative w-full md:bg-none flex items-center h-full px-8 md:px-[15px] md:mx-14 md:top-0 md:justify-start lg:mx-16 xl:mx-[156px]">
          <div className="pt-40 max-w-full md:max-w-40 text-white pb-10 md:py-20 md:items-start md:justify-start lg:max-w-md">
            <p className="uppercase text-xs lg:text-sm text-[rgb(229,177,173)] my-2">
              Saatnya berlangganan dan nikmati layanan berkualitas dari IndiHome
            </p>
            <h3 className="font-bold text-2xl lg:text-3xl mb-4">
              Nikmati sinyal kuat di mana saja, bebas telepon hingga ribuan
              menit, dan banyak keuntungan lainnya.
            </h3>
            <ul className="flex flex-col mt-8 space-y-4 justify-start">
              <li className="flex items-center space-x-4">
                <FontAwesomeIcon
                  icon={faCircle}
                  width={20}
                  className="text-white"
                />
                <p className="text-sm lg:text-base">Kuota tanpa batas*</p>
              </li>
              <li className="flex items-center gap-4">
                <FontAwesomeIcon
                  icon={faCircle}
                  width={20}
                  className="text-white"
                />
                <p className="text-sm lg:text-base">
                  Bebas atur langganan sesuai anggaran
                </p>
              </li>
              <li className="flex items-center basis-5 gap-4">
                <FontAwesomeIcon
                  icon={faCircle}
                  width={20}
                  className="text-white"
                />
                <p className="text-sm lg:text-base">
                  Bebas biaya langganan beragam aplikasi menarik
                </p>
              </li>
              <li className="flex items-center gap-4">
                <FontAwesomeIcon
                  icon={faCircle}
                  width={20}
                  className="text-white"
                />
                <p className="text-sm lg:text-base">Layanan pelanggan 24 jam</p>
              </li>
            </ul>
          </div>
        </div>
      </section>

      <section className="mt-24 md:mt-0 py-10 bg-white overflow-hidden">
        <div className=" max-w-6xl px-4 mx-auto lg:flex-row sm:px-6 lg:px-8">
          <p className="uppercase text-sm text-[#999999] mb-8">
            Penawaran Terbaru
          </p>
        </div>
        <Slider category="offers" className="mx-16" />
      </section>

      <section className="py-16 overflow-hidden">
        <div className="max-w-6xl px-4 mx-auto lg:flex-row sm:px-6 lg:px-8">
          <div className="flex flex-col justify-center text-left items-start">
            <p className="uppercase text-sm text-[#999999]">
              Kami siap membantu anda
            </p>
            <h1 className="my-3 text-3xl font-bold text-[#343536]">
              Anda memiliki pertanyaan seputar IndiHome?
            </h1>
            <p className="text-gray-500">
              Kami siap membantu dan menjawab pertanyaan seputar layanan
              IndiHome.
            </p>
            <div className="flex justify-center md:justify-start lg:justify-center w-full">
              <div className="grid grid-cols-2 md:flex gap-4 md:items-start lg:items-center justify-center mt-8">
                <div className="w-44 h-full">
                  <div className="card-helper red">
                    <div className="flex justify-end">
                      <Image
                        src="/assets/img/internet-icon.png"
                        width={50}
                        height={50}
                        alt="Internet"
                      />
                    </div>
                    <div className="mt-4">
                      <h3 className="text-white text-lg font-bold">Internet</h3>
                      <p className="text-[#c0c4c6] text-sm mt-2 lg:block hidden">
                        Pertanyaan seputar paket internet
                      </p>
                    </div>
                  </div>
                </div>
                <div className="w-44 h-full">
                  <div className=" card-helper blue">
                    <div className="flex justify-end">
                      <Image
                        src="/assets/img/tv-icon.png"
                        width={50}
                        height={50}
                        alt="Internet"
                      />
                    </div>
                    <div className="mt-4">
                      <h3 className="text-white text-lg font-bold">TV</h3>
                      <p className="text-[#c0c4c6] text-sm mt-2 lg:block hidden">
                        Pertanyaan seputar TV interaktif dan minipack
                      </p>
                    </div>
                  </div>
                </div>
                <div className="w-44 h-full">
                  <div className="card-helper green">
                    <div className="flex justify-end">
                      <Image
                        src="/assets/img/telepon-icon.png"
                        width={50}
                        height={50}
                        alt="Internet"
                      />
                    </div>
                    <div className="mt-4">
                      <h3 className="text-white text-lg font-bold">
                        Telephone
                      </h3>
                      <p className="text-[#c0c4c6] text-sm mt-2 lg:block hidden">
                        Pertanyaan seputar langganan telephone
                      </p>
                    </div>
                  </div>
                </div>
                <div className="w-44 h-full">
                  <div className="card-helper aqua">
                    <div className="flex justify-end">
                      <Image
                        src="/assets/img/addons-icon.png"
                        width={50}
                        height={50}
                        alt="Internet"
                      />
                    </div>
                    <div className="mt-4">
                      <h3 className="text-white text-lg font-bold">Add-On</h3>
                      <p className="text-[#c0c4c6] text-sm mt-2 lg:block hidden">
                        Pertanyaan seputar layanan add-on
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
