import { useEffect, useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCircleQuestion,
  faArrowLeft,
  faEyeSlash,
  faEye,
} from '@fortawesome/free-solid-svg-icons';
import { useForm } from 'react-hook-form';
import InvalidModal from '../../../components/Modal/InvalidRegisterModal';

const Register = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [showModal, setShowModal] = useState(false);
  const [inputFullName, setFullName] = useState('');
  const [inputPhoneNumber, setPhoneNumber] = useState('');
  const [inputEmail, setEmail] = useState('');
  const [inputPassword, setPassword] = useState('');

  const [showPassword, setShowPassword] = useState(false);
  const [nameCondition, setNameCondition] = useState(true);
  const [phoneCondition, setPhoneCondition] = useState(true);
  const [emailCondition, setEmailCondition] = useState(true);
  const [passwordCondition, setPasswordCondition] = useState(true);

  const onSubmit = async () => {
    await fetch('http://localhost:8080/api/v1/users/register', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        name: inputFullName,
        password: inputPassword,
        email: inputEmail,
        notelp: inputPhoneNumber,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.code === 200) {
          window.location.href = '/login';
        } else {
          setShowModal(true);
        }
      });
  };

  const errorInvalidName = () => {
    return (
      <p className="text-red-500 text-xs">
        Pastikan nama yang Anda tulis sesuai dengan KTP
      </p>
    );
  };
  const errorInvalidNumber = () => {
    return (
      <p className="text-red-500 text-xs">
        Mohon cantumkan nomor ponsel yang aktif
      </p>
    );
  };
  const errorInvalidEmail = () => {
    return (
      <p className="text-red-500 text-xs">
        Mohon cantumkan email yang aktif address
      </p>
    );
  };
  const errorInvalidPassword = () => {
    if (!passwordCondition) {
      return (
        <span className="text-red-500 text-xs block mt-2">
          Minimal 8 karakter terdiri dari gabungan huruf dan angka dengan
          minimal 1 huruf kapital
        </span>
      );
    } else if (errors.password?.type === 'pattern') {
      setPasswordCondition(false);
      return (
        <span className="text-red-500 text-xs block mt-2">
          Minimal 8 karakter terdiri dari gabungan huruf dan angka dengan
          minimal 1 huruf kapital
        </span>
      );
    } else {
      return null;
    }
  };

  const regexFirstSpace = (value) => {
    return value.charAt(0) === ' ';
  };

  const regexName = (value) => {
    return /^[a-zA-Z ]+$/.test(value);
  };

  const regexEmail = (value) => {
    return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value);
  };

  const regexPhoneNumber = (value) => {
    return /^08[0-9]*$/.test(value);
  };

  const regexPassword = (value) => {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(value);
  };

  useEffect(() => {
    document.body.className = 'body-bg';
  });

  return (
    <div data-testid="heading">
      <nav className="pt-9 pb-7 mb-7 hidden sm:block">
        <div className="flex justify-center">
          <Link href="/">
            <Image
              src="/assets/img/logo_indihome.png"
              width={148}
              height={148}
              alt="IndiHome"
            />
          </Link>
        </div>
      </nav>
      <div className="flex justify-center">
        <div className="md:w-[490px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem]">
          <div className="mb-">
            <div className="flex justify-between mb-2">
              <Link href="/">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  width={20}
                  className="text-red-500 text-lg"
                />
              </Link>
              <Link href="/help/internet/detail/1">
                <FontAwesomeIcon
                  icon={faCircleQuestion}
                  className="text-red-500 text-lg"
                  width={20}
                />
              </Link>
            </div>

            <Image
              src="/assets/img/header_register.png"
              width={102}
              height={102}
              alt="Register Logo"
              className="mb-4"
            />
            <h1 className="text-3xl font-bold text-[#353637]">
              Pendaftaran Baru
            </h1>
          </div>

          <form onSubmit={handleSubmit(onSubmit)} className="mt-8">
            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2"
                htmlFor="fullname"
              >
                Nama Lengkap
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-sm"
                id="fullname"
                {...register('fullname', {
                  required: true,
                })}
                type="text"
                placeholder="Masukkan nama lengkap"
                onBlur={(e) => {
                  if (
                    e.target.value === '' ||
                    regexFirstSpace(e.target.value) ||
                    regexName(e.target.value) === false
                  ) {
                    setNameCondition(false);
                  } else {
                    setNameCondition(true);
                  }
                  setFullName(e.target.value);
                }}
              />
              {!nameCondition ? errorInvalidName() : null}
            </div>

            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2"
                htmlFor="nohp"
              >
                Nomor Ponsel
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline text-sm placeholder:text-sm"
                id="nohp"
                type="number"
                {...register('nohp', {
                  required: true,
                  pattern:
                    /^((\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}|(\d{4}-?){3}\d{4})$/,
                })}
                placeholder="Masukkan nomor ponsel"
                onBlur={(e) => {
                  if (
                    e.target.value === '' ||
                    regexFirstSpace(e.target.value) ||
                    regexPhoneNumber(e.target.value) === false
                  ) {
                    setPhoneCondition(false);
                  } else {
                    setPhoneCondition(true);
                  }
                  setPhoneNumber(e.target.value);
                }}
              />
              {!phoneCondition ? errorInvalidNumber() : null}
            </div>

            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2"
                htmlFor="email"
              >
                Email
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-sm"
                id="email"
                {...register('email', {
                  required: true,
                })}
                type="email"
                placeholder="Masukkan alamat email"
                onBlur={(e) => {
                  if (
                    e.target.value === '' ||
                    regexFirstSpace(e.target.value) ||
                    regexEmail(e.target.value) === false
                  ) {
                    setEmailCondition(false);
                  } else {
                    setEmailCondition(true);
                  }
                  setEmail(e.target.value);
                }}
              />
              {!emailCondition ? errorInvalidEmail() : null}
            </div>

            <div className="mb-4">
              <label
                className="block text-[#999] text-xs font-normal tracking-widest mb-2"
                htmlFor="password"
              >
                Kata Sandi
              </label>
              <div className="relative">
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-sm"
                  id="password"
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Buat kata sandi baru"
                  {...register('password', {
                    required: true,
                    pattern: /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
                  })}
                  onBlur={(e) => {
                    if (
                      e.target.value === '' ||
                      regexPassword(e.target.value) === false
                    ) {
                      setPasswordCondition(false);
                    } else {
                      setPasswordCondition(true);
                    }
                    setPassword(e.target.value);
                  }}
                />
                <div className="absolute right-0 top-0 mt-[.45rem] ">
                  {showPassword ? (
                    <FontAwesomeIcon
                      icon={faEye}
                      className="text-md text-red-500 cursor-pointer mr-2 mt-[0.11rem]"
                      width={22}
                      onClick={() => setShowPassword(!showPassword)}
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faEyeSlash}
                      className="text-md text-[#999] cursor-pointer mr-2 mt-[0.11rem]"
                      width={22}
                      onClick={() => setShowPassword(!showPassword)}
                    />
                  )}
                </div>
                <span
                  className={`${
                    !passwordCondition ? 'hidden' : 'block'
                  } text-[#848484] text-xs mt-2`}
                >
                  Minimal 8 karakter terdiri dari gabungan huruf dan angka
                  dengan minimal 1 huruf kapital
                </span>
                {errorInvalidPassword()}
              </div>
            </div>

            <div className="flex justify-center mt-8">
              <button
                className="bg-red-500 hover:bg-red-700 disabled:bg-red-500 py-3 rounded-lg focus:outline-none focus:shadow-outline w-full transition duration-100 ease-in-out"
                type="submit"
                disabled={
                  inputFullName !== '' &&
                  inputPhoneNumber !== '' &&
                  inputEmail !== '' &&
                  inputPassword !== ''
                    ? false
                    : true
                }
                onClick={() => {
                  handleSubmit(onSubmit);
                }}
              >
                <span
                  className={`uppercase font-bold text-sm ${
                    inputFullName !== '' &&
                    inputPhoneNumber !== '' &&
                    inputEmail !== '' &&
                    inputPassword !== ''
                      ? 'text-white'
                      : 'text-[#f69891]'
                  }`}
                >
                  Daftar Sekarang
                </span>
              </button>
            </div>
          </form>
        </div>
      </div>
      <InvalidModal show={showModal} onClose={() => setShowModal(false)} />
    </div>
  );
};

export default Register;
