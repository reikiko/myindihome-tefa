import Link from 'next/link';
import Layout from '../components/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image';
import { useEffect } from 'react';
export default function EditPage() {
  useEffect(() => {
    document.body.className = 'body-bg';
  });
  return (
    <Layout>
      <div className="flex justify-center">
        <div className="md:w-[700px] sm:max-w-full bg-white shadow-2xl rounded-lg px-5 pt-6 pb-8 md:mb-24 sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <h1 className="text-3xl font-bold text-[#353637] mx-1 my-2">
              Pengaturan Paket Langganan
            </h1>
          </div>

          <div className="flex justify-center mt-[5rem]">
            <div className="flex flex-col items-center">
              <Image
                src="/assets/img/header_failed.png"
                width={200}
                height={200}
                alt="failed"
              />
              <p className="text-center">
                Anda tidak memiliki langganan aktif apapun.
              </p>
              <Link
                href="/shop/internet/package"
                className="btn-primary py-4 px-5 uppercase font-bold text-sm rounded-lg mt-8 mb-12"
              >
                Cari Paket Internet
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
