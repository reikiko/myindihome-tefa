import '../styles/globals.css';
import '../styles/Navbar.css';
import { Toaster } from 'react-hot-toast';
import Head from 'next/head';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          type="text/css"
          href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin"
        />
        <title>
          IndiHome | New Excitement | Internet Rumah dan Cepat Hingga 300Mbps
        </title>
      </Head>
      <Toaster position="top-right" />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
