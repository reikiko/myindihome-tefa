import { useState, useEffect } from 'react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import Layout from '../components/Layout';

export default function ManageAccount() {
  const [checked, setChecked] = useState(true);
  const handleToggle = (e) => {
    setChecked(e.target.checked);
  };

  useEffect(() => {
    document.body.className = 'body-bg';
  });
  return (
    <Layout>
      <section className="flex justify-center">
        <div className="md:w-[505px] sm:max-w-full bg-white shadow-xl rounded-lg px-5 pt-6 pb-8 md:mb-4 sm:mx-4 body-bg-mobile sm:max-h-[50rem] md:mt-4">
          <div className="mb-7">
            <div className="flex justify-between">
              <Link href="/profile">
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  className="text-red-500 text-lg"
                  width={15}
                />
              </Link>
            </div>

            <div className="ml-1 mt-2">
              <h1 className="text-3xl font-bold text-[#353637] ">
                Pengaturan Notifikasi
              </h1>
              <p className="mt-3">
                Atur notifikasi masuk yang ingin Anda terima
              </p>
            </div>
          </div>

          <div className="px-3 my-4">
            <div className="flex flex-col items-center">
              <div className="flex justify-between w-full border-b border-[#E5E5E5] py-4">
                <span className="text-gray-900">Pembelian Baru</span>
                <label className="inline-flex relative items-center cursor-pointer">
                  <input
                    type="checkbox"
                    className="sr-only peer"
                    checked={checked}
                    onChange={handleToggle}
                  />
                  <div className="w-[1.9rem] h-4 bg-gray-200 border border-gray-500 peer-focus:outline-none peer-focus:ring-1 rounded-full peer peer-focus:ring-red-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[6px] after:left-[3px] after:bg-gray-500 after:rounded-full after:h-3 after:w-3 after:transition-all peer-checked:bg-red-600 after:peer-checked:bg-gray-200 peer-checked:border-red-500"></div>
                </label>
              </div>
              <div className="flex justify-between w-full border-b border-[#E5E5E5] py-4">
                <span className="text-gray-900">Pembelian Tagihan</span>
                <label className="inline-flex relative items-center cursor-pointer">
                  <input
                    type="checkbox"
                    className="sr-only peer"
                    checked={checked}
                    onChange={handleToggle}
                  />
                  <div className="w-[1.9rem] h-4 bg-gray-200 border border-gray-500 peer-focus:outline-none peer-focus:ring-1 rounded-full peer peer-focus:ring-red-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[6px] after:left-[3px] after:bg-gray-500 after:rounded-full after:h-3 after:w-3 after:transition-all peer-checked:bg-red-600 after:peer-checked:bg-gray-200 peer-checked:border-red-500"></div>
                </label>
              </div>
              <div className="flex justify-between w-full border-b border-[#E5E5E5] py-4">
                <span className="text-gray-900">Pembelian Akun</span>
                <label className="inline-flex relative items-center cursor-pointer">
                  <input
                    type="checkbox"
                    className="sr-only peer"
                    checked={checked}
                    onChange={handleToggle}
                  />
                  <div className="w-[1.9rem] h-4 bg-gray-200 border border-gray-500 peer-focus:outline-none peer-focus:ring-1 rounded-full peer peer-focus:ring-red-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[6px] after:left-[3px] after:bg-gray-500 after:rounded-full after:h-3 after:w-3 after:transition-all peer-checked:bg-red-600 after:peer-checked:bg-gray-200 peer-checked:border-red-500"></div>
                </label>
              </div>
              <div className="flex justify-between w-full border-b border-[#E5E5E5] py-4">
                <span className="text-gray-900">Promosi dan Penawaran</span>
                <label className="inline-flex relative items-center cursor-pointer">
                  <input
                    type="checkbox"
                    className="sr-only peer"
                    checked={checked}
                    onChange={handleToggle}
                  />
                  <div className="w-[1.9rem] h-4 bg-gray-200 border border-gray-500 peer-focus:outline-none peer-focus:ring-1 rounded-full peer peer-focus:ring-red-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[6px] after:left-[3px] after:bg-gray-500 after:rounded-full after:h-3 after:w-3 after:transition-all peer-checked:bg-red-600 after:peer-checked:bg-gray-200 peer-checked:border-red-500"></div>
                </label>
              </div>
              <div className="flex justify-between w-full border-b border-[#E5E5E5] py-4">
                <span className="text-gray-900">Pembelian Baru</span>
                <label className="inline-flex relative items-center cursor-pointer">
                  <input
                    type="checkbox"
                    className="sr-only peer"
                    checked={checked}
                    onChange={handleToggle}
                  />
                  <div className="w-[1.9rem] h-4 bg-gray-200 border border-gray-500 peer-focus:outline-none peer-focus:ring-1 rounded-full peer peer-focus:ring-red-500 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[6px] after:left-[3px] after:bg-gray-500 after:rounded-full after:h-3 after:w-3 after:transition-all peer-checked:bg-red-600 after:peer-checked:bg-gray-200 peer-checked:border-red-500"></div>
                </label>
              </div>
            </div>
          </div>

          <div className="flex justify-center mt-8">
            <button className="btn-primary text-white text-sm font-bold p-3 rounded-lg w-full uppercase">
              Simpan
            </button>
          </div>
        </div>
      </section>
    </Layout>
  );
}
