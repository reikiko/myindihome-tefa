export default function CardSearch({ data }) {
  const numberDot = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  return data.map((item) => (
    <div className="block" key={item.id}>
      <div className="hidden lg:block bg-white relative shadow-md rounded-lg justify-center max-w-[350px] h-full items-center">
        <div className="flex items-center justify-center">
          <div className="absolute -top-5 mx-auto">
            <div className="flex flex-col justify-center items-center w-[90px] h-[70px] rounded-lg transition duration-200 transform hover:scale-110 badges-blue">
              <h3 className="font-bold text-2xl">{item.channel}</h3>
              <span className="text-white opacity-75 text-sm">Channel</span>
            </div>
          </div>
        </div>
        <div className="pt-16 px-5 relative">
          <div className="flex flex-col gap-1">
            <h1 className="font-bold text-center text-sm uppercase tracking-wide text-gray-900">
              {item.name}
            </h1>
            <h3 className="text-center text-lg font-bold">
              Rp{numberDot(item.price)}
              <small className="text-center text-xs font-light text-gray-400">
                /month
              </small>
            </h3>
            <div className="overflow-hidden max-w-xs break-words pt-5">
              <p className="text-center text-sm text-gray-400 para-ellip">
                {item.description}
              </p>
            </div>
          </div>
        </div>
        <div className="flex items-center justify-center">
          <div className="my-6">
            <a
              href="#"
              className="text-white block rounded-lg text-center font-medium leading-6 px-6 py-3 bg-[#EE3225] hover:bg-[#db1e11]"
            >
              <span className="font-bold uppercase">Berlangganan</span>
            </a>
          </div>
        </div>
      </div>

      <div className="block lg:hidden">
        <div className="grid grid-cols-1 bg-white shadow-md rounded-md sm:w-[550px] md:w-[700px] h-full">
          <div className="p-4 flex flex-row">
            <div className="flex flex-col justify-center p-2 items-center w-[70px] h-[70px] rounded-lg transition duration-200 transform hover:scale-110 badges-blue">
              <h3 className="font-bold text-2xl">{item.channel}</h3>
              <span className="text-white text-sm opacity-75">Channel</span>
            </div>
            <div className="ml-4 flex flex-col justify-center items-start">
              <h1 className="font-bold text-start text-sm tracking-wide text-gray-900">
                {item.name}
              </h1>
              <p className="text-start text-xs para-ellip">
                {item.description}
              </p>
            </div>
          </div>
          <div className="px-5 pb-4 grid grid-cols-2 items-center place-content-between">
            <div className="justify-self-start">
              <h3 className="text-center text-lg font-bold">
                Rp{numberDot(item.price)}
                <small className="text-center text-xs font-light text-gray-400">
                  /month
                </small>
              </h3>
            </div>
            <div className="my-5 justify-self-end">
              <a
                href="#"
                className="text-white max-w-max rounded-lg text-center font-medium leading-6 px-6 py-3 bg-[#EE3225] hover:bg-[#db1e11]"
              >
                <span className="font-bold uppercase">Berlangganan</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  ));
}
