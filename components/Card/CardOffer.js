import Image from 'next/image';
import Link from 'next/link';

export default function CardOffer({ title, image, link }) {
  return (
    <div className="lg:w-60 md:w-64">
      <Link href={link}>
        <Image
          className="max-w-[220px] md:max-h-fit rounded-lg mb-4 hover:shadow-lg"
          src={`/assets/img/${image}`}
          alt="Cloud Storage"
          width={500}
          height={500}
        />
      </Link>
      <div className="hidden md:block">
        <p className="mb-2">{title}</p>
        <Link
          href={link}
          className="mb-3 font-bold tracking-wider text-sm text-red-500 uppercase"
        >
          Lihat Detail
        </Link>
      </div>
    </div>
  );
}
