import Image from 'next/image';
import Link from 'next/link';

export default function CardOffer({ image, link, alt }) {
  return (
    <div>
      <Link href={link}>
        <Image
          className="rounded-lg mb-4 hover:shadow-lg w-80"
          src={`/assets/img/${image}`}
          alt={alt}
          width={1000}
          height={1000}
        />
      </Link>
    </div>
  );
}
