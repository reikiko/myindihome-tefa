import Image from 'next/image';

export default function CardPackage({ data }) {
  const numberDot = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  return data.map((item) => (
    <div id="packages" key={item.id} className="">
      <div className="bg-white relative shadow-md rounded-lg justify-center hidden h-[510px] lg:flex">
        <div className="flex items-center justify-center gap-3 absolute -top-10 mx-auto">
          <div className="flex flex-col justify-center items-center w-[85px] h-[85px] rounded-lg transition duration-200 transform hover:scale-110 badges-red">
            <h3 className="font-bold text-3xl">{item.speed}</h3>
            <span className="text-white opacity-75">Mbps</span>
          </div>
          <div className="flex flex-col justify-center items-center w-[85px] h-[85px] rounded-lg transition duration-200 transform hover:scale-110 badges-blue">
            <h3 className="font-bold text-3xl">{item.channel}</h3>
            <span className="text-white opacity-75">Channel</span>
          </div>
          {item.menit !== 0 ? (
            <div className="flex flex-col justify-center items-center w-[85px] h-[85px] rounded-lg transition duration-200 transform hover:scale-110 badges-green">
              <h3 className="font-bold text-3xl">{item.menit}</h3>
              <span className="text-white opacity-75">Menit</span>
            </div>
          ) : (
            ''
          )}
        </div>

        <div className="pt-16 px-5 relative">
          <div className="flex flex-col gap-1">
            <h1 className="font-bold text-center text-sm uppercase tracking-wide text-gray-900">
              {item.name}
            </h1>
            <h3 className="text-center text-lg font-bold">
              Rp{numberDot(item.price)}
              <small className="text-center text-xs font-light text-gray-400">
                /bulan
              </small>
            </h3>
            <div className="overflow-hidden max-w-xs break-words pt-5">
              <p className="text-center text-md text-gray-400 para-ellip">
                {item.description}
              </p>
            </div>
          </div>
          <div className="border-t mt-11">
            <div className="flex flex-col gap-1 pt-5">
              <h1 className="text-center text-sm uppercase tracking-wide text-gray-900">
                Bonus Layanan
              </h1>
              <div className="overflow-hidden max-w-xs break-words">
                <p className="text-center text-md text-gray-400 para-ellip">
                  Dapatkan langganan bonus dari berbagai aplikasi dan game!
                </p>
              </div>
            </div>
            <div className="flex flex-row gap-4 justify-center items-center py-5">
              {item.gambar.map((bonus, idx) => (
                <Image
                  key={idx}
                  src={bonus}
                  width={64}
                  height={64}
                  alt={bonus.title}
                />
              ))}
            </div>
          </div>
          <div className="my-5 absolute bottom-0 px-16">
            <a
              href="#"
              className="text-white block rounded-lg text-center font-medium leading-6 px-6 py-3 bg-[#EE3225] hover:bg-[#db1e11]"
            >
              <span className="font-bold uppercase">Berlangganan</span>
            </a>
          </div>
        </div>
      </div>
      <div className="block lg:hidden">
        <div className="grid grid-cols-1 bg-white shadow-md rounded-md sm:w-[550px] md:w-[700px] h-full">
          <div className="p-4 flex flex-row">
            <div className="flex flex-col justify-center p-2 items-center w-[70px] h-[70px] rounded-lg transition duration-200 transform hover:scale-110 badges-red">
              <h3 className="font-bold text-2xl">{item.speed}</h3>
              <span className="text-white text-sm opacity-75">Mbps</span>
            </div>
            <div className="ml-4 flex flex-col justify-center items-start">
              <h1 className="font-bold text-start text-sm tracking-wide text-gray-900">
                {item.name}
              </h1>
              <p className="text-start text-xs para-ellip">
                {item.description}
              </p>
            </div>
          </div>
          <div className="px-5 pb-4 grid grid-cols-2 items-center place-content-between">
            <div className="justify-self-start">
              <h3 className="text-center text-lg font-bold">
                Rp{numberDot(item.price)}
                <small className="text-center text-xs font-light text-gray-400">
                  /bulan
                </small>
              </h3>
            </div>
            <div className="my-5 justify-self-end">
              <a
                href="#"
                className="text-white max-w-max rounded-lg text-center font-medium leading-6 px-6 py-3 bg-[#EE3225] hover:bg-[#db1e11]"
              >
                <span className="font-bold uppercase">Berlangganan</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  ));
}
