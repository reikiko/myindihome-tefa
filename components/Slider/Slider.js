import React from 'react';
import CardOffer from '../Card/CardOffer';
import CardLatest from '../Card/CardLatest';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const itemsOffers = [
  {
    title: 'Cloud Storage',
    image: 'offers_1cloud_storage.png',
    link: '/',
  },
  {
    title: 'Smooa',
    image: 'offers_2smooa.png',
    link: '/',
  },
  {
    title: 'Pijar Belajar',
    image: 'offers_3pijar.png',
    link: '/',
  },
  {
    title: 'Benefit Voucher Game',
    image: 'offers_4benefitgame.png',
    link: '/',
  },
  {
    title: 'Upgrade Speed',
    image: 'offers_5upgrade_speed.png',
    link: '/',
  },
  {
    title: 'Hybrid Box STB Tambahan',
    image: 'offers_6hybrid_box.png',
    link: '/',
  },
];
const itemsLatest = [
  {
    image: 'latest_1worldcup.jpg',
    link: '/',
    alt: 'World Cup',
  },
  {
    image: 'latest_2smooa.jpg',
    link: '/',
    alt: 'Smooa',
  },
  {
    image: 'latest_3netflix.jpg',
    link: '/',
    alt: 'Netflix',
  },
  {
    image: 'latest_4indihome.jpg',
    link: '/',
    alt: 'Indihome',
  },
  {
    image: 'latest_5internet.jpg',
    link: '/',
    alt: 'Internet',
  },
  {
    image: 'latest_6internet2.jpg',
    link: '/',
    alt: 'Internet',
  },
  {
    image: 'latest_7suap.jpg',
    link: '/',
    alt: 'Suap',
  },
];

export default function SliderComponent({ category }) {
  const settings = {
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ],
  };

  return (
    <div className="max-w-6xl px-4 mx-auto lg:flex-row sm:px-6 lg:px-8">
      <Slider {...settings}>
        {category === 'offers'
          ? itemsOffers.map((item, index) => (
              <CardOffer
                key={index}
                title={item.title}
                image={item.image}
                link={item.link}
              />
            ))
          : itemsLatest.map((item, index) => (
              <CardOffer
                key={index}
                title={item.title}
                image={item.image}
                link={item.link}
              />
            ))}
      </Slider>
    </div>
  );
}
