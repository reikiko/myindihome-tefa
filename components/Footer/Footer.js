import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import {
  faFacebookF,
  faInstagram,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import Link from 'next/link';

export default function Footer() {
  return (
    <footer className="bg-[#333435] flex flex-col pt-16 h-fit sticky top-[100vh] ">
      <div className="flex footer-content w-max lg:w-[73rem] px-4 md:mx-auto justify-between lg:px-8 overflow-x-hidden">
        <div className="flex flex-col gap-y-3 pr-12">
          <p className="text-[#999] text-sm uppercase">Tautan Cepat</p>
          <Link href="/home" className="text-white">
            Berita terkini
          </Link>
          <Link href="/home" className="text-white">
            Cara melihat tagihan
          </Link>
          <Link href="/home" className="text-white">
            Ajukan Pengaduan Layanan
          </Link>
          <Link href="/home" className="text-white">
            Cara menggunakan aplikasi
          </Link>
          <Link href="/home" className="text-white">
            Jadilah mitra Kami
          </Link>
        </div>
        <div className="flex flex-col">
          <p className="text-[#999] text-sm uppercase">
            Ikuti kami dan unduh aplikasi di
          </p>
          <div className="flex space-x-4 my-4">
            <a href="https://apps.apple.com/id/app/myindihome/id1119407221">
              <Image
                src="/assets/img/button_appstore.png"
                width={150}
                height={50}
                alt="Appstore"
              />
            </a>
            <a href="https://play.google.com/store/apps/details?id=com.telkom.indihome.external">
              <Image
                src="/assets/img/button_playstore.png"
                width={150}
                height={50}
                alt="Playstore"
              />
            </a>
          </div>
          <div className="flex mt-2">
            <div className="flex justify-center items-center w-8 h-8 bg-white mr-3 text-xl text-[#333435] rounded-lg hover:bg-[#3c5a99] hover:text-white transition duration-500 ease-in-out">
              <a href="https://facebook.com/indihome">
                <FontAwesomeIcon icon={faFacebookF} width={20} height={20} />
              </a>
            </div>
            <div className="flex justify-center items-center w-8 h-8 bg-white mr-3 text-xl text-[#333435] rounded-lg hover:bg-gradient-to-tr hover:from-yellow-500 hover:via-pink-600 hover:to-indigo-600 hover:text-white transition duration-500 ease-in-out">
              <a href="https://instagram.com/indihome">
                <FontAwesomeIcon icon={faInstagram} width={20} height={20} />
              </a>
            </div>
            <div className="flex justify-center items-center w-8 h-8 bg-white mr-3 text-xl text-[#333435] rounded-lg hover:bg-[#38a1f3] hover:text-white transition duration-500 ease-in-out">
              <a href="https://twitter.com/indihome">
                <FontAwesomeIcon icon={faTwitter} width={20} height={20} />
              </a>
            </div>
            <div className="flex justify-center items-center w-8 h-8 bg-white mr-3 text-xl text-[#333435] rounded-lg hover:text-white transition duration-500 ease-in-out">
              <a href="tel:147">
                <FontAwesomeIcon icon={faPhone} width={20} height={20} />
              </a>
            </div>
          </div>
        </div>
        <div className="flex flex-col">
          <p className="text-[#999] text-sm ">Didukung oleh</p>
          <a href="https://telkom.co.id/">
            <Image
              src="/assets/img/logo_telkom.png"
              width={160}
              height={50}
              alt="Telkom"
              className=""
            />
          </a>
        </div>
      </div>
      <div className="bg-[#191919] w-full mt-12 py-[1.45rem] text-white overflow-hidden">
        <div className="flex flex-col md:flex-row md:w-[72rem] px-4 mx-auto justify-between sm:px-6 lg:px-8">
          <p className="text-sm text-[#d1d1d1]">
            &copy; 2019 PT Telekomunikasi Indonesia (Persero), Tbk.
          </p>
          <div className="flex space-x-4">
            <Link href="/privacy-policy" className="text-sm hover:border-b">
              Kebijakan Privasi
            </Link>
            <Link href="/terms-conditions" className="text-sm hover:border-b">
              Syarat & Ketentuan
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
}
