/* eslint-disable @next/next/no-html-link-for-pages */
import { useState, useRef, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import LoginModal from '../Modal/LoginModal';
import { useRouter } from 'next/router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMagnifyingGlass,
  faClockRotateLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import styled from 'styled-components';

export default function Navbar() {
  const router = useRouter();
  const [isLogin, setIsLogin] = useState(true);
  const [open, setOpen] = useState(false);
  const node = useRef();
  const [name, setName] = useState('');

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setIsLogin(true);
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const decodedData = JSON.parse(window.atob(base64));
      setName(decodedData.name);
    } else {
      setIsLogin(false);
    }
  }, []);

  const StyledBurger = styled.button`
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 0.4rem;
    width: 2rem;
    height: 2rem;
    background: transparent;
    border: none;
    cursor: pointer;
    padding: 0;
    z-index: 10;

    &:focus {
      outline: none;
    }

    div {
      width: 1.75rem;
      height: 0.15rem;
      background: ${({ open }) => (open ? 'red' : 'red')};
      border-radius: 10px;
      -webkit-transition: all 0.5s linear;
      transform-origin: 1px;

      :first-child {
        transform: ${({ open }) => (open ? 'rotate(45deg)' : 'rotate(0)')};
      }

      :nth-child(2) {
        opacity: ${({ open }) => (open ? '0' : '1')};
        transform: ${({ open }) =>
          open ? 'translateX(20px)' : 'translateX(0)'};
      }

      :nth-child(3) {
        transform: ${({ open }) => (open ? 'rotate(-45deg)' : 'rotate(0)')};
      }
    }
  `;

  const Burger = ({ open, setOpen }) => {
    return (
      <StyledBurger open={open} onClick={() => setOpen(!open)}>
        <div />
        <div />
        <div />
      </StyledBurger>
    );
  };

  const StyledMenu = styled.nav`
    display: ${({ open }) => (open ? 'flex' : 'none')};
    flex-direction: column;
    flex-grow: 1;
    justify-content: start;
    background: white;
    transform: ${({ open }) => (open ? 'translateX(0)' : 'translateX(-100%)')};
    height: ${({ open }) => (open ? '100vh' : '0')};
    text-align: left;
    transition: transform 0.3s ease-in-out;
    }

    item{
      padding: 1rem 1rem 1rem 0.5rem;
      display: flex;
      justify-content: space-between;
    }

    a{
      font-size: 1.25rem;
      color: #4d4d4d;


    }
  `;

  const Menu = ({ open }) => {
    return (
      <StyledMenu open={open}>
        <item>
          <Link href="/home">Beranda</Link>
          <FontAwesomeIcon
            icon={faChevronRight}
            width={10}
            className="text-red-500 hover:text-red-600 transition duration-500 ease-in-out cursor-pointer"
          />
        </item>
        <item>
          <Link href="/history/bill">Riwayat</Link>
          <FontAwesomeIcon
            icon={faChevronRight}
            width={10}
            className="text-red-500 hover:text-red-600 transition duration-500 ease-in-out cursor-pointer"
          />
        </item>
        <item>
          <Link href="/shop">Beli</Link>
          <FontAwesomeIcon
            icon={faChevronRight}
            width={10}
            className="text-red-500 hover:text-red-600 transition duration-500 ease-in-out cursor-pointer"
          />
        </item>
        <item className="">
          <Link href="/help">Bantuan</Link>
          <FontAwesomeIcon
            icon={faChevronRight}
            width={10}
            className="text-red-500 hover:text-red-600 transition duration-500 ease-in-out cursor-pointer"
          />
        </item>
        <item className="">
          {isLogin ? (
            <button className="btn btn-primary flex-grow">
              <Link href="/profile" className="!text-white">
                Profile
              </Link>
            </button>
          ) : (
            <LoginModal />
          )}
        </item>
      </StyledMenu>
    );
  };

  return (
    <nav className="fixed-top sticky navbar p-4 bg-white shadow-sm overflow-hidden">
      <div className="container relative p-4 flex flex-wrap justify-between items-center h-full sm:mx-20 md:mx-14 lg:mx-16 xl:mx-[150px]">
        <Link href="/" className="navbar-brand">
          <Image
            src="/assets/img/logo_indihome.png"
            width={100}
            height={20}
            alt="IndiHome"
          />
        </Link>
        <div className="hidden md:block">
          <ul className="nav-menu">
            <li className="nav-item">
              <Link
                href="/home"
                className={
                  router.pathname == '/home'
                    ? 'nav-link text-red-500'
                    : 'nav-link hover:text-red-500 transition duration-500 ease-in-out'
                }
              >
                Beranda
              </Link>
            </li>
            <li className="nav-item">
              <Link
                href="/history/bill"
                className={
                  router.pathname == '/history/bill'
                    ? 'nav-link text-red-500'
                    : 'nav-link hover:text-red-500 transition duration-500 ease-in-out'
                }
              >
                Riwayat
              </Link>
            </li>
            <li className="nav-item">
              <Link
                href="/shop"
                className={
                  router.pathname == '/shop'
                    ? 'nav-link text-red-500'
                    : 'nav-link hover:text-red-500 transition duration-500 ease-in-out'
                }
              >
                Beli
              </Link>
            </li>
            <li className="nav-item">
              <Link
                href="/help"
                className={
                  router.pathname == '/help'
                    ? 'nav-link text-red-500'
                    : 'nav-link hover:text-red-500 transition duration-500 ease-in-out'
                }
              >
                Bantuan
              </Link>
            </li>
            <li className="nav-item mx-2">
              <Link href="/search-result">
                <FontAwesomeIcon
                  icon={faMagnifyingGlass}
                  width={20}
                  className="text-red-500 hover:text-red-600 transition duration-500 ease-in-out cursor-pointer"
                />
              </Link>
            </li>
            {!isLogin ? (
              <li className="nav-item">
                <LoginModal />
              </li>
            ) : (
              <>
                <li className="nav-item mx-2">
                  <FontAwesomeIcon
                    icon={faEnvelope}
                    width={20}
                    className="hover:text-red-500 transition duration-500 ease-in-out cursor-pointer"
                  />
                </li>
                <li className="nav-item mx-2">
                  <Link href="/history/bill">
                    <FontAwesomeIcon
                      icon={faClockRotateLeft}
                      width={16}
                      className="hover:text-red-500 transition duration-500 ease-in-out cursor-pointer"
                    />
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/profile">
                    <div className="flex justify-center items-center w-10 h-10 bg-[#F2F2F2] border border-gray-300 rounded-full">
                      <p className="">{name[0]}</p>
                    </div>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
        <div className="flex flex-row items-center gap-3 md:hidden">
          <ul>
            <li className="nav-item mx-2">
              <Link href="/search-result">
                <FontAwesomeIcon
                  icon={faMagnifyingGlass}
                  width={20}
                  className="text-red-500 hover:text-red-600 transition duration-500 ease-in-out cursor-pointer"
                />
              </Link>
            </li>
          </ul>
          <div ref={node}>
            <Burger open={open} setOpen={setOpen} />
          </div>
        </div>
      </div>
      <div className="block md:hidden flex-grow">
        <Menu open={open} setOpen={setOpen} />
      </div>
    </nav>
  );
}
