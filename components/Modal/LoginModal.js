/* eslint-disable @next/next/no-html-link-for-pages */
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image';
import Link from 'next/link';

export default function LoginModal() {
  const [show, setShow] = useState(false);
  return (
    <>
      <button
        className="btn btn-primary flex-grow"
        onClick={() => setShow(!show)}
      >
        Masuk/Daftar
      </button>
      {show ? (
        <>
          <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
          <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50">
            <div className="max-w-lg bg-white shadow-xl rounded-lg px-8 pt-6 pb-8 mb-4">
              <button className="float-right" onClick={() => setShow(!show)}>
                <FontAwesomeIcon
                  icon={faTimes}
                  width="20"
                  className="text-red-500 text-2xl"
                />
              </button>
              <div className="mb-8">
                <Image
                  src="/assets/img/header_login.png"
                  width={100}
                  height={20}
                  alt="Login"
                />
                <h3 className="text-2xl font-bold text-[#353637]">
                  Masuk atau Daftar untuk Melanjutkan
                </h3>
                <p className="text-gray-500">
                  Masuk atau buat akun untuk melanjutkan proses pembelian. Ini
                  hanya akan memakan waktu beberapa menit
                </p>
              </div>
              <div className="flex justify-between">
                <Link
                  href="/login"
                  className="border border-red-500 focus:ring-1 focus:outline-none focus:ring-red-600 font-bold rounded-lg text-sm text-center w-[48%] py-4 uppercase"
                >
                  Masuk
                </Link>

                <Link
                  href="/register/account-details"
                  className="bg-[#ee3124] hover:bg-[#db1e11] border hover:border-[#cf1c10] font-bold rounded-lg text-sm text-center w-[48%] py-4 uppercase text-white transition duration-100 ease-in-out"
                >
                  Daftar
                </Link>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}
