export default function InvalidRegisterModal({ show, onClose }) {
  const handleClose = (e) => {
    e.preventDefault();
    onClose();
  };

  const handleLogout = () => {
    localStorage.removeItem('token');
    window.location.href = '/login';
  };
  return show ? (
    <>
      <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
      <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50">
        <div className="w-[30rem] bg-white shadow-xl rounded-lg px-8 pt-6 pb-8 mb-4">
          <div className="text-center">
            <h1 className="text-xl font-semibold">
              Apakah Anda yakin ingin Keluar dari myIndiHome?
            </h1>
            <p className="pt-2 pb-5">
              Setelah keluar, Anda harus masuk lagi untuk mengakses fitur-fitur
              dalam aplikasi myIndiHome
            </p>
          </div>
          <div className="flex justify-between">
            <button
              className="border border-red-500 focus:ring-1 focus:outline-none focus:ring-red-600 font-bold rounded-lg text-sm text-center w-[48%] py-4 uppercase cursor-pointer"
              onClick={handleLogout}
            >
              Ya
            </button>
            <button
              className="bg-[#ee3124] hover:bg-[#db1e11] border hover:border-[#cf1c10] font-bold rounded-lg text-sm text-center w-[48%] py-4 uppercase text-white transition duration-100 ease-in-out"
              onClick={handleClose}
            >
              Tidak
            </button>
          </div>
        </div>
      </div>
    </>
  ) : null;
}
