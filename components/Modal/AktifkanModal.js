export default function AktifkanModal({ show, onClose }) {
  const handleClose = (e) => {
    e.preventDefault();
    onClose();
  };

  return show ? (
    <>
      <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
      <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50">
        <div className="w-[500px] bg-white shadow-xl rounded-lg px-6 pt-6 pb-8 mb-4">
          <div className="text-center mb-8">
            <h3 className="text-xl font-bold text-[#353637]">Aktifkan Paket</h3>
            <p className="mt-3 text-sm">
              Maaf, anda harus menyelesaikan pemasangan paket IndiHome Anda
              terlebih dahulu
            </p>
          </div>
          <div className="">
            <button
              className="btn-primary p-3 rounded-lg font-bold text-sm w-full"
              onClick={handleClose}
            >
              OK
            </button>
          </div>
        </div>
      </div>
    </>
  ) : null;
}
