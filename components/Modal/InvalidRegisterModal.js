import Link from 'next/link';

export default function InvalidRegisterModal({ show, onClose }) {
  const handleClose = (e) => {
    e.preventDefault();
    onClose();
  };
  return show ? (
    <>
      <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
      <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50">
        <div className="max-w-xl bg-white shadow-xl rounded-lg px-8 pt-6 pb-8 mb-4">
          <div className="text-center">
            <h1 className="text-xl font-semibold">
              Nomor telepon atau email telah terdaftar
            </h1>
            <p className="pt-2 pb-5">Apakah Anda mau masuk sekarang?</p>
          </div>
          <div className="flex justify-between">
            <p
              className="border border-red-500 focus:ring-1 focus:outline-none focus:ring-red-600 font-bold rounded-lg text-sm text-center w-[48%] py-4 uppercase cursor-pointer"
              onClick={handleClose}
            >
              Coba Lagi
            </p>

            <Link
              href="/login"
              className="bg-[#ee3124] hover:bg-[#db1e11] border hover:border-[#cf1c10] font-bold rounded-lg text-sm text-center w-[48%] py-4 uppercase text-white transition duration-100 ease-in-out"
            >
              Masuk
            </Link>
          </div>
          <div className="flex justify-center mt-4">
            <Link
              href="/help/internet/detail/1"
              className="font-bold text-sm text-center w-[48%] pt-2 uppercase"
            >
              Minta Bantuan
            </Link>
          </div>
        </div>
      </div>
    </>
  ) : null;
}
