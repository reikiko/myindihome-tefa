import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';

export default function ComingSoonModal({ show, onClose }) {
  const handleClose = (e) => {
    e.preventDefault();
    onClose();
  };

  return show ? (
    <>
      <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
      <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50">
        <div className="w-[500px] bg-white shadow-xl rounded-lg px-6 pt-6 pb-8 mb-4">
          <button className="float-right" onClick={handleClose}>
            <FontAwesomeIcon
              icon={faTimes}
              width="15"
              className="text-red-500 text-2xl"
            />
          </button>
          <div className="mb-8">
            <Image
              src="/assets/img/logo_upcoming.svg"
              width="100"
              height="100"
              alt="Upcoming"
            />
            <h3 className="text-3xl font-bold text-[#353637]">
              Terima kasih untuk ketertarikan Anda
            </h3>
            <p className="text-gray-500 mt-3">
              Paket ini akan tersedia segera!
            </p>
          </div>
          <div className="">
            <button
              className="btn-primary p-3 rounded-lg font-bold text-sm w-full"
              onClick={handleClose}
            >
              OK
            </button>
          </div>
        </div>
      </div>
    </>
  ) : null;
}
