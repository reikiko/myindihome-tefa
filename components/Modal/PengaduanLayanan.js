/* eslint-disable @next/next/no-html-link-for-pages */
import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';

export default function PengaduanLayanan() {
  const [show, setShow] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState('');

  useEffect(() => {
    localStorage.setItem('category', selectedCategory);
  }, [selectedCategory]);

  return (
    <>
      <button
        className="btn btn-primary mt-8 Pengaduan layanan"
        onClick={() => setShow(!show)}
      >
        Pengaduan layanan
      </button>
      {show ? (
        <>
          <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
          <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center z-50">
            <div className="max-w-xl bg-white shadow-xl rounded-lg px-8 pt-6 pb-8 mb-4">
              <button className="float-right" onClick={() => setShow(!show)}>
                <FontAwesomeIcon
                  icon={faTimes}
                  width="20"
                  className="text-red-500 text-2xl"
                />
              </button>
              <div className="mb-8">
                <h3 className="text-3xl font-bold text-[#353637]">
                  Pengaduan Layanan
                </h3>
                <p className="text-gray-500 mt-4">
                  Mohon maaf atas ketidaknyamanannya. Mohon laporkan masalah
                  atau gangguan tersebut di sini agar Kami dapat segera
                  menindaklanjutinya.
                </p>
              </div>
              <div className="mb-8">
                <label
                  htmlFor="category"
                  className="block text-xs font-medium text-gray-500 uppercase tracking-widest mb-3"
                >
                  Kategori
                </label>
                <select
                  id="category"
                  name="category"
                  className="mt-1 block w-full p-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-gray-500 focus:border-gray-500 focus:border-2 sm:text-sm"
                  onChange={(e) => setSelectedCategory(e.target.value)}
                >
                  <option disabled selected>
                    Pilih Kategori
                  </option>
                  <option value="Administrasi">Administrasi dan Tagihan</option>
                  <option value="Telepon">Telepon</option>
                  <option value="TV">TV</option>
                  <option value="Internet">Internet</option>
                </select>
              </div>
              <div className="">
                <Link
                  href="/help/internet/detail/ticket"
                  className={`bg-[#ee3124] hover:bg-[#db1e11] border hover:border-[#cf1c10] font-bold rounded-lg text-xs text-center p-4 uppercase transition duration-100 ease-in-out ${
                    selectedCategory === ''
                      ? 'text-[#f69891] pointer-events-none'
                      : 'text-white'
                  }`}
                >
                  Lanjut
                </Link>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}
